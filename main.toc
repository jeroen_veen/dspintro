\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Workshop preface}{5}{chapter.1}%
\contentsline {section}{\numberline {1.1}Introduction}{5}{section.1.1}%
\contentsline {section}{\numberline {1.2}Organization}{6}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}DSP miniproject}{6}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Quiz}{6}{subsection.1.2.2}%
\contentsline {subsection}{\numberline {1.2.3}Schedule}{7}{subsection.1.2.3}%
\contentsline {section}{\numberline {1.3}Matlab}{8}{section.1.3}%
\contentsline {section}{\numberline {1.4}More information}{8}{section.1.4}%
\contentsline {chapter}{\numberline {2}The basics}{9}{chapter.2}%
\contentsline {section}{\numberline {2.1}Introduction}{9}{section.2.1}%
\contentsline {section}{\numberline {2.2}Digital signals}{10}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Signal sampling}{11}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Discrete-time signal representation}{12}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Quantization}{14}{subsection.2.2.3}%
\contentsline {subsection}{\numberline {2.2.4}Aliasing}{15}{subsection.2.2.4}%
\contentsline {section}{\numberline {2.3}Digital signal processing systems}{16}{section.2.3}%
\contentsline {section}{\numberline {2.4}Further reading}{17}{section.2.4}%
\contentsline {chapter}{\numberline {3}The discrete-time domain}{19}{chapter.3}%
\contentsline {section}{\numberline {3.1}Introduction}{19}{section.3.1}%
\contentsline {section}{\numberline {3.2}Linear time-invariant (LTI) systems}{21}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Linearity}{21}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Time-invariance}{22}{subsection.3.2.2}%
\contentsline {section}{\numberline {3.3}Difference equations}{22}{section.3.3}%
\contentsline {section}{\numberline {3.4}Impulse response and convolution}{24}{section.3.4}%
\contentsline {subsection}{\numberline {3.4.1}Causality}{24}{subsection.3.4.1}%
\contentsline {subsection}{\numberline {3.4.2}Stability}{25}{subsection.3.4.2}%
\contentsline {section}{\numberline {3.5}Finite impulse response (FIR) filters}{25}{section.3.5}%
\contentsline {section}{\numberline {3.6}Infinite impulse response (IIR) filters}{27}{section.3.6}%
\contentsline {section}{\numberline {3.7}Further reading}{29}{section.3.7}%
\contentsline {chapter}{\numberline {4}The z-domain}{31}{chapter.4}%
\contentsline {section}{\numberline {4.1}Introduction}{31}{section.4.1}%
\contentsline {section}{\numberline {4.2}The z-transform}{31}{section.4.2}%
\contentsline {section}{\numberline {4.3}Transfer functions}{34}{section.4.3}%
\contentsline {section}{\numberline {4.4}The z-plane}{37}{section.4.4}%
\contentsline {section}{\numberline {4.5}Return to the time-domain}{40}{section.4.5}%
\contentsline {section}{\numberline {4.6}Stability}{42}{section.4.6}%
\contentsline {section}{\numberline {4.7}Further reading}{43}{section.4.7}%
\contentsline {chapter}{\numberline {5}The frequency domain}{45}{chapter.5}%
\contentsline {section}{\numberline {5.1}Introduction}{45}{section.5.1}%
\contentsline {section}{\numberline {5.2}From the z-domain to the frequency domain}{45}{section.5.2}%
\contentsline {section}{\numberline {5.3}Frequency responses}{47}{section.5.3}%
\contentsline {section}{\numberline {5.4}Aliasing revisited}{51}{section.5.4}%
\contentsline {subsection}{\numberline {5.4.1}Aliasing frequency}{52}{subsection.5.4.1}%
\contentsline {subsection}{\numberline {5.4.2}Folding}{53}{subsection.5.4.2}%
\contentsline {section}{\numberline {5.5}The discrete Fourier transform (DFT)}{53}{section.5.5}%
\contentsline {section}{\numberline {5.6}Return to the time-domain}{55}{section.5.6}%
\contentsline {section}{\numberline {5.7}Further reading}{57}{section.5.7}%
\contentsline {chapter}{\numberline {6}Discrete-time filter design}{59}{chapter.6}%
\contentsline {section}{\numberline {6.1}Introduction}{59}{section.6.1}%
\contentsline {subsubsection}{Frequency response}{60}{section*.1}%
\contentsline {subsubsection}{Phase and group delay}{60}{section*.2}%
\contentsline {subsubsection}{Causality}{61}{section*.3}%
\contentsline {subsubsection}{Stability}{61}{section*.4}%
\contentsline {subsubsection}{Locality}{62}{section*.5}%
\contentsline {subsubsection}{Computational complexity}{62}{section*.6}%
\contentsline {section}{\numberline {6.2}FIR filter design}{62}{section.6.2}%
\contentsline {subsection}{\numberline {6.2.1}Window design method}{62}{subsection.6.2.1}%
\contentsline {subsection}{\numberline {6.2.2}Parks-McClellan method}{65}{subsection.6.2.2}%
\contentsline {subsection}{\numberline {6.2.3}Implementation}{67}{subsection.6.2.3}%
\contentsline {section}{\numberline {6.3}IIR filter design}{67}{section.6.3}%
\contentsline {subsection}{\numberline {6.3.1}Bilinear transform}{69}{subsection.6.3.1}%
\contentsline {subsubsection}{Frequency warping}{70}{section*.7}%
\contentsline {subsection}{\numberline {6.3.2}Digital biquad filters}{71}{subsection.6.3.2}%
\contentsline {subsection}{\numberline {6.3.3}Implementation}{74}{subsection.6.3.3}%
\contentsline {section}{\numberline {6.4}Further reading}{74}{section.6.4}%
