\chapter{The discrete-time domain} \label{chap:discrete-time_domain}
\section{Introduction}

In signal processing, a filter is a device or process that removes some unwanted components or features from a signal. Most often, this means removing some frequencies or frequency bands. Filters are widely used in electronics and telecommunication, in radio, television, audio recording, radar, control systems, music synthesis, image processing, and computer graphics. Digital signal processing allows the inexpensive construction of a wide variety of filters. A digital filter is a system that performs mathematical operations on a sampled, discrete-time (DT) signal to reduce or enhance certain aspects of that signal. This is in contrast to the other major type of electronic filter, the analog filter, which is an electronic circuit operating on continuous-time analog signals. In digital signal processing, the continuous-time (CT) signal is sampled and an analog-to-digital converter turns the signal into a stream of numbers. It is very important to limit the range of frequencies in the input signal for faithful representation in the DT signal, since then the sampling theorem guarantees that no information about the continuous-time CT signal is lost. A computer program running on a CPU or a specialized DSP (or less often running on a hardware implementation) calculates an output number stream. This output can be converted to a signal by passing it through a digital-to-analog converter. 

In this workshop we will consider discrete-time filters, which is a subset of the digital filters. So we will not consider quantization. A discrete-time filter is a system that performs mathematical operations on a sampled, discrete-time signal to reduce or enhance certain aspects of that signal. A filter can be represented by a block diagram, which can then be used to derive a sample processing algorithm to implement the filter with hardware instructions. A typical block diagram of a DT filter is depicted below, where an output sample, or filtered value, $y[n]$, is related to an input sample, or incoming raw value, $x[n]$, via impulse response, $h[n]$. 
\begin{figure}[ht]
    \centering
    \includegraphics[scale=1]{figures/DT_filter_block_diagram.eps}
    \caption{Blockdiagram of a discrete-time filter.} \label{fig:DT_filter}
\end{figure}

There are two categories of digital filter: the recursive filter and the nonrecursive filter. These are often referred to as infinite impulse response (IIR) filters and finite impulse response (FIR) filters, respectively. A variety of mathematical techniques may be employed to analyze the behavior of a given DT filter. Mathematical analysis of the filter can describe how it will respond to any input. A filter is characterized by its transfer function, impulse response, or equivalently, its difference equation. We will discuss these in the workshop. As such, designing a filter consists of developing specifications appropriate to the problem (for example, a second-order low pass filter with a specific cut-off frequency), and then producing a transfer function which meets the specifications.

DT filters are built of three mathematical operators: addition/subtraction, multiplication by a constant, and storage/delay, which can easily be realized on a CPU, DSP or in hardware. In the table below the different operators are shown, together with their block-diagram representation which you will typically find in text books.

\def\arraystretch{2.5}%  1 is the default, change whatever you need
\begin{table}[h!]
    \centering
    \begin{tabular}{|l|l|c|}
        \hline
        Addition & $y[n] = x_1[n] + x_2[n]$ & \includegraphics[scale=0.75]{figures/adder.eps} \\ \hline 
        Multiplication & $y[n] = a x[n]$ & \includegraphics[scale=0.75]{figures/multiplyer.eps}\\ \hline
        Delay & $y[n] = x[n-1]$ & \includegraphics[scale=0.75]{figures/delay.eps}\\ \hline
    \end{tabular}
    \caption{Basic components of discrete-time filters.} \label{tab:filter_components}
\end{table}

The design of digital filters is a deceptively complex topic. Although filters are easily understood and calculated, the practical challenges of their design and implementation are significant and are the subject of much advanced research. DT filters composed of these basic components will result in linear and time-invariant (also known as shift invariance) systems, which will be discussed in more detail in the next section.

\begin{exmp}
    A moving average (rolling average or running average) is a calculation to analyze data points by creating a series of averages of different subsets of the full data set.  A moving average is commonly used with time series data to smooth out short-term fluctuations and highlight longer-term trends or cycles. The threshold between short-term and long-term depends on the application, and the parameters of the moving average will be set accordingly. A moving average (MA) filter is a very simple FIR filter. It is sometimes called a boxcar filter. The filter coefficients, $b_0, \ldots, b_P$ of the MA filter are found via the following equation:
    \begin{equation} \label{eq:MA_coeff}
        \begin{matrix}
        b_i = \frac{1}{P+1}, &  0 \le i \le P
        \end{matrix} .
    \end{equation} 
    To provide a more specific example, we select the filter order: $P=2$. The block diagram of this basic discrete-time filter is given in the figure below.
    \begin{figure}[ht]
        \centering
        \includegraphics[scale=1]{figures/MA_filter_block_diagram.eps}
        \caption{Blockdiagram of a Moving Average (MA) filter with $P=2$.} \label{fig:Blockdiagram_MA_filter}
    \end{figure}

    So, what does this MA filter do to a signal? In Figure \ref{fig:Response_MA_filter} an example is plotted. As you can see this DT system smooths the data a little, but not much since in this case only 3 samples are averaged to produce an output sample.
    \begin{figure}[ht]
        \centering
        \includegraphics[width=0.6\linewidth]{figures/MA_response.eps}
        \caption{Response of a Moving Average (MA) filter with $P=2$ to an input sequence.} \label{fig:Response_MA_filter}
    \end{figure}
\end{exmp}


\section{Linear time-invariant (LTI) systems}
Linear time-invariant theory, commonly known as LTI system theory, comes from applied mathematics and has direct applications in signal processing, control theory, and other technical areas. It investigates the response of a linear and time-invariant system to an arbitrary input signal. LTI system theory is good at describing many important systems. Most LTI systems are considered "easy" to analyze, at least compared to the time-varying and/or nonlinear case. A good example of LTI systems are electrical circuits that can be made up of resistors, capacitors, and inductors. Trajectories of these systems are commonly measured and tracked as they move through time (e.g. an acoustic waveform), but in applications like image processing, the LTI systems also have trajectories in spatial dimensions. Note that in the case of generic discrete-time (i.e., sampled) systems, linear shift-invariant is the corresponding term, however we will stick to the more common term, time-invariant.

\subsection{Linearity}
Linearity means that the relationship between the input and the output of the system is a linear map: If input $x_1[n]$ produces response $y_1[n]$, and input $x_2[n]$ produces response $y_2[n]$, then the scaled and summed input $a_1 x_1[n] + a_2 x_2[n]$, produces the scaled and summed response $a_1 y_1[n] + a_2 y_2[n]$, where $a_1$ and $a_2$ are real numbers.
\begin{figure}[ht]
    \centering
    \includegraphics[scale=1]{figures/linearity_block_diagram.eps}
    \caption{A DT filter is linear.} \label{fig:Linearity}
\end{figure}
It follows that this can be extended to an arbitrary number of terms, and so for real numbers $c_1, c_2, \ldots, c_k$ the input $\sum_k c_k x[n]$ produces the output $\sum_k c_k y[n]$. Linear filters satisfy the superposition principle, i.e. if an input is a weighted linear combination of different signals, the output is a similarly weighted linear combination of the corresponding output signals.

Note that mathematical notation uses a symbol that compactly represents summation of many similar terms: the summation symbol, $\sum$, the capital Greek letter Sigma. This is defined as:
\begin{equation*} 
    \sum_{i=a}^b r_i = r_a + r_{a+1} + + r_{a+2} + \cdots + r_{b-1} + r_b ,
\end{equation*}
where $i$ represents the index of summation; $r_i$ is an indexed variable representing each successive term in the series; $a$ is the lower bound of summation, and $b$ is the upper bound of summation. The $i = a$ under the summation symbol means that the index $i$ starts out equal to $a$. The index, $i$, is incremented by 1 for each successive term, stopping when $i = b$. Here is an example showing the summation of squares:
\begin{equation*} 
    \sum_{i=3}^6 i^2  = 3^2 + 4^2 + 5^2 + 6^2 =86 .
\end{equation*}
So, summation is the addition of a sequence of numbers; the result is their sum or total. 

\subsection{Time-invariance}
Time-invariance means that whether we apply an input to the system now or T seconds from now, the output will be identical except for a time delay of T seconds. That is, if the output due to input x[n] is y[n], then the output due to input x[n-a] is y[n-a], where a is a natural number, i.e. a non-negative integer, specifies the number of samples delayed.
\begin{figure}[ht]
    \centering
    \includegraphics[scale=1]{figures/time_invariance_block_diagram.eps}
    \caption{A DT filter is time-invariant.} \label{fig:Time-invariance}
\end{figure}
A time-invariant filter has constant properties over time; other filters such as adaptive filters change in time, but they are beyond the scope of this workshop.

\section{Difference equations}
Digital filters are often described and implemented in terms of the difference equation that defines how the output signal is related to the input signal:
\begin{align} \label{eq:diff_eq}
    y[n] = \frac{1}{a_0} & \left ( b_{0}x[n]+b_{1}x[n-1]+\cdots +b_{P}x[n-P] \right . \\
        & \left . - a_{1}y[n-1]-a_{2}y[n-2]-\cdots -a_{Q}y[n-Q] \right )
\end{align}

% \begin{equation} \label{eq:diff_eq}
%     \begin{matrix} 
%         y[n] = &\frac {1}{a_{0}} \left ( b_{0}x[n]+b_{1}x[n-1]+\cdots +b_{P}x[n-P] \right ) \\
%         &-\frac {1}{a_{0}} \left (a_{1}y[n-1]-a_{2}y[n-2]-\cdots -a_{Q}y[n-Q] \right )
%     \end{matrix} ,
% \end{equation} 
where:
\begin{itemize}
    \item P is the feedforward filter order
    \item $b_{{i}}$ for $0 \le i \le P$ are the feedforward filter coefficients
    \item Q is the feedback filter order
    \item $a_j$ for $0 \le j \le Q$ are the feedback filter coefficients
\end{itemize}
The difference equation shows how to compute the next output sample, $y[n]$, in terms of the past outputs, $y[n-j]$, the present input, $x[n]$, and the past inputs, $x[n-i]$.

A more condensed form of the difference equation is: 
\begin{equation} \label{eq:diff_eq_cond}
    y[n] = \frac{1}{a_0} \left ( \sum_{i=0}^P b_i x[n-i] - \sum_{j=1}^Q a_j y[n-j] \right ) .
\end{equation}
The general form of the difference equation, when rearranged, becomes:
\begin{equation} \label{eq:diff_eq_bal}
    \sum_{j=0}^Q a_j y[n-j] = \sum_{i=0}^P b_i x[n-i] .
\end{equation}
In the next chapter it will become clear, why Equation 8 is useful.

\begin{exmp}
    Again, let’s look at the example of the 3-point moving average filter of Figure \ref{fig:Blockdiagram_MA_filter}. The forward filtering coefficients are given by Equation \ref{eq:MA_coeff}. There are no feedback filter coefficients. The difference equation of an MA filter with $P=2$ equals:
    \begin{equation*}
        \begin{matrix}
            y[n] & = \frac{1}{3} x[n] + \frac{1}{3} x[n-1] + \frac{1}{3} x[n-2] \\
            & = \frac{1}{3} \left ( x[n] + x[n-1] + x[n-2] \right ) . 
        \end{matrix} 
    \end{equation*}
    In plain terms, for example, as used by a programmer implementing the above equation in code, it can be described as follows:
    \begin{itemize}
        \item y[n] = the current filtered (output) value
        \item x[n] = the current raw input value
        \item x[n-1] = the last raw input value
        \item x[n-2] = the 2nd-to-last raw input value 
    \end{itemize}
    Now, consider a signal with sample values $(-1,2,1)$ being applied to this filter. The input, $x[n]$, output, $y[n]$, and delay line elements, $x[n-1]$ and $x[n-2]$, are tabelized in Table \ref{tab:MA_filter_content}.
    \begin{table}[h!]
        \centering
        \begin{tabular}[hb]{l|l|l|l|l}
            n & x[n] & x[n-1] & x[n-2] & y[n]\\
            \hline
            0 & -1 & 0 & 0 & $-\frac{1}{3}$\\
            1 & 2 &  -1 & 0 & $\frac{1}{3}$\\
            2 & 1 & 2 &  -1 & $\frac{2}{3}$\\
            3 & 0 & 1 & 2 & 1\\
            4 & 0 & 0 & 1 & $\frac{1}{3}$\\
            5 & 0 & 0 & 0 & 0 \\
            6 & $\cdots$ & $\cdots$ & $\cdots$ & $\cdots$ 
        \end{tabular}
        \caption{Sample values in a second order MA filter.} \label{tab:MA_filter_content}
    \end{table}
\end{exmp}


\section{Impulse response and convolution}

The fundamental result in LTI system theory is that any LTI system can be characterized entirely by a single function called the system's impulse response. The output of the system is simply the convolution of the input to the system with the system's impulse response. So, the output of the filter to any given input may be calculated by convolving the input signal with the impulse response. This method of analysis is often called the time domain point-of-view. The same result is true of discrete-time linear shift-invariant systems in which signals are discrete-time samples, and convolution is defined on sequences.

For a DT system, the impulse response, $h[n]$, characterizes the system completely. For any input sequence, $x[n]$, the output sequence, $y[n]$, can be calculated in terms of the input and the impulse response:
\begin{align}\label{eq:discrete_convolution}
    y[n] & =  x[n] * h[n] \nonumber \\
    & = \sum_{k=-\infty}^{+\infty} x[k] h[n-k] \nonumber \\
    & = \sum_{k=-\infty}^{+\infty} h[k] x[n-k] 
    ,
\end{align}
where $y[n] =  x[n] * h[n]$ denotes discrete-time convolution. The term convolution refers to both the result $y[n]$ and to the process of computing it. Convolution can be interpreted as proportional to a weighted average of the input $x[k]$. The weighting function is $h[-k]$, simply shifted by amount $n$. As the sample number, $n$, changes, the weighting function emphasizes different parts of the input function. Equivalently, the system response to an impulse at $n=0$ is a "time" reversed copy of the unshifted weighting function. An excellent visual explanation of convolution can be found \href{https://en.wikipedia.org/wiki/Convolution}{here}.

For the special case of the system response, $y[n]$, to the Kronecker delta function, $x[n]=\delta[n]$ (Equation \ref{eq:kron_delta}), the output sequence is equal to the impulse response of the system:
\begin{align}\label{eq:imp_resp}
    y[n] & =  \delta[n] * h[n] \nonumber \\
    & = \sum_{k=-\infty}^{+\infty} \delta[k] h[n-k] = h [n].
\end{align}
This result can be easily verified by realizing that there is only a single sample number, $n$, for which $x[n]=\delta[n]$ is non-zero, thus only a single term in the summation remains, i.e. for $k=0$.

\subsection{Causality}

In order to process signals in real-time a system must be causal. For a causal system, the impulse response of the system must use only the present and past values of the input to determine the output. In other words, when $h[n]$ is zero for all negative $n$, the system is said to be causal,
\begin{equation*} 
    \begin{array}{ll}
            h[n] = 0, & \text{if } n < 0 .
    \end{array}
\end{equation*}
So for the practical systems considered in this workshop, causality applies, and convolution (Equation \ref{eq:discrete_convolution}) is computed as
\begin{align*}
    y[n] & =  x[n] * h[n] \\
    & = \sum_{k=0}^{+\infty} h[k] x[n-k] 
    .
\end{align*}
A causal filter uses only previous samples of the input or output signals; while a non-causal filter uses future input samples. A non-causal filter can usually be changed into a causal filter by adding a delay to it. Note that similar rules apply to either discrete or continuous cases. Unlike CT systems, non-causal DT systems can be realized, but that is beyond our current scope.

\subsection{Stability}
Another important property of the systems considered in this workshop is stability. In signal processing, specifically control theory, bounded-input, bounded-output (BIBO) stability is a form of stability for linear signals and systems that take inputs. If a system is BIBO stable, then the output will be bounded for every input to the system that is bounded. In practical systems, this is really necessary since unbounded outputs may not be computable.

For a discrete time LTI system, the condition for BIBO stability is that the impulse response be absolutely summable, i.e.
\begin{equation} \label{eq:DT_stability}
    \sum_{k=-\infty}^{\infty} \left | h[k] \right | < \infty ,
\end{equation}
where $\left | \cdot \right |$ denotes absolute value.

A stable filter produces an output that converges to a constant value with time, or remains bounded within a finite interval. An unstable filter can produce an output that grows without bounds, with bounded or even zero input. Note that non-stable systems can be built and can be useful in many circumstances, but that is beyond our scope.

\section{Finite impulse response (FIR) filters}

A finite impulse response (FIR) filter is a filter whose impulse response is of finite duration, because it settles to zero in finite time. This is in contrast to infinite impulse response (IIR) filters, which may continue to respond indefinitely (although usually decaying). 

For a causal discrete-time FIR filter of order $P$, each value of the output sequence is a weighted sum of the most recent input values:
\begin{align*}
    y[n] & = b_{0}x[n]+b_{1}x[n-1]+\cdots +b_{P}x[n-P] \\
    & = \sum_{i=0}^P b_i x[n-i] ,
\end{align*}
which is equivalent to the general difference Equation \ref{eq:diff_eq} with only feedforward terms.
Again $b_{{i}}$ for $0 \le i \le P$ are the feedforward filter coefficients.
The $x[n-i]$ in these terms are commonly referred to as taps, based on the structure of a tapped delay line that in many implementations or block diagrams provides the delayed inputs to the multiplication operations. One may speak of a 5th order/6-tap filter, for instance. 

Obviously, the MA filter of Figure \ref{fig:Blockdiagram_MA_filter} is an example of an FIR filter with constant coefficients. A generalized block diagram of a FIR filter of order $P$ is depicted in Figure \ref{fig:Blockdiagram_FIR_filter}. Note that this is a block-diagram of the so-called direct implementation, since other forms of implementation are possible, but these are beyond the scope of the workshop.

\begin{figure}[ht]
    \centering
    \includegraphics[scale=1]{figures/FIR_block_diagram.eps}
    \caption{A direct form discrete-time FIR filter of order $P$. The top part is an $P$-stage delay line with $P+1$ taps.} \label{fig:Blockdiagram_FIR_filter}
\end{figure}

The impulse response of an FIR filter is defined as nonzero over a finite duration, hence its name. Including zeroes, the impulse response is:
\begin{align} \label{eq:IR_FIR}
    h[n] & = \sum_{i=0}^P b_i \delta[n-i] \\
    & = \left \{ 
            \begin{array}{ll}
                b_n & \text{for } 0 \leq n \leq P\\
                0 & \text{otherwise}
            \end{array}
        \right . \nonumber
\end{align}
Note that it is trivial to make an acausal FIR system causal by adding delays. 
\begin{exmp}
    Now, let’s return to the MA filter example. The impulse response is depicted in Figure \ref{fig:IR_MA_filter}, which explains why it is sometimes called a boxcar filter.
    \begin{figure}[ht]
        \centering
        \includegraphics[width=0.6\linewidth]{figures/MA_IR_response.eps}
        \caption{Impulse response of a Moving Average (MA) filter with $P=2$.} \label{fig:IR_MA_filter}
    \end{figure}
\end{exmp}
An FIR filter has a number of useful properties which sometimes make it preferable to an infinite impulse response (IIR) filter. FIR filters:
\begin{itemize}
    \item Require no feedback. This means that any rounding errors are not compounded by summed iterations. The same relative error occurs in each calculation. This also makes implementation simpler.
    \item Are inherently stable, since the output is a sum of a finite number of multiples of the input values, so can be no greater than $ \sum \left |b_{i} \right |$ times the largest value appearing in the input.
    \item Can easily be made to be linear phase (constant group delay vs frequency) by making the coefficient sequence symmetric. This property is sometimes desired for phase-sensitive applications, for example data communications, seismology, crossover filters, and mastering.
    \item Can more easily be designed, for instance, to match a particular frequency response requirement. This is particularly true when the requirement is not one of the usual cases (high-pass, low-pass, notch, etc.), which have been studied and optimized for analog filters.
\end{itemize}

The main disadvantage of FIR filters is that considerably more computation power in a general purpose processor is required compared to an IIR filter with similar sharpness or selectivity, especially when low frequency (relative to the sample rate) cutoffs are needed. However, many digital signal processors provide specialized hardware features to make FIR filters approximately as efficient as IIR for many applications. 


\section{Infinite impulse response (IIR) filters}
Infinite impulse response (IIR) filters are distinguished by having an impulse response which does not become exactly zero past a certain point but continues indefinitely. This is in contrast to a finite impulse response (FIR) in which the impulse response $h[n]$ does become exactly zero after $P+1$ samples, thus being of finite duration. 

In practice, the impulse response, even of IIR systems, usually approaches zero and can be neglected past a certain point. However the physical systems which give rise to IIR or FIR responses are dissimilar, and therein lies the importance of the distinction. For instance, analog electronic filters composed of resistors, capacitors, and/or inductors (and perhaps linear amplifiers) are generally IIR filters. On the other hand, discrete-time filters (usually digital filters) based on a tapped delay line employing no feedback are necessarily FIR filters. The capacitors (or inductors) in the analog filter have a "memory" and their internal state never completely relaxes following an impulse (assuming the classical model of capacitors and inductors where quantum effects are ignored). But in the latter case, after an impulse has reached the end of the tapped delay line, the system has no further memory of that impulse and has returned to its initial state; its impulse response beyond that point is exactly zero. 
Although almost all analog electronic filters are IIR, digital filters may be either IIR or FIR. The presence of feedback in the topology of a discrete-time filter (such as the block diagram shown in Figure \ref{fig:Blockdiagram_IIR_filter} generally creates an IIR response. For IIR filters, the filter order is equal to the number of delay elements in the filter structure. Generally, the larger the filter order, the better the frequency magnitude response performance.
\begin{figure}[ht]
    \centering
    \includegraphics[scale=1]{figures/IIR_block_diagram.eps}
    \caption{A direct form discrete-time IIR filter.} \label{fig:Blockdiagram_IIR_filter}
\end{figure}

For a causal discrete-time IIR filter, each value of the output sequence is a weighted sum of the most recent input values, $x[n-i]$, and past output values, $y[n-j]$. For convenience, the difference equation is repeated here:
\begin{align*}
    y[n] = \frac{1}{a_0} & \left ( b_{0}x[n]+b_{1}x[n-1]+\cdots +b_{P}x[n-P] \right . \\
    & \left . a_{1}y[n-1]-a_{2}y[n-2]-\cdots -a_{Q}y[n-Q] \right ) \\
    = \frac{1}{a_0} & \left ( \sum_{i=0}^P b_i x[n-i] - \sum_{j=1}^Q a_j y[n-j] \right ) , 
\end{align*}
where $a_j$ for $0 \le j \le Q$ are the feedback coefficients and $b_i$ for $0 \le i \le P$ are the feedforward filter coefficients. The difference equation shows how to compute the next output sample, $y[n]$, in terms of the past outputs, $y[n-j]$, the present input, $x[n]$, and the past inputs, $x[n-i]$. 

Considering that in most IIR filter designs coefficient $a_0=1$, the IIR filter difference equation takes the more traditional form: 
\begin{equation*}
    y[n] = \sum_{i=0}^P b_i x[n-i] - \sum_{j=1}^Q a_j y[n-j] ,
\end{equation*}
where the output gain is unity, as depicted also in Figure \ref{fig:Blockdiagram_IIR_filter}

In contrast to the FIR filter of Equation \ref{eq:IR_FIR}, a difference equation with arbitrary feedback coefficients is not easily solved, and a  IIR filter impulse response cannot be expressed generally. As a consequence, it is not trivial to guarantee stability of the system. For these reasons, in the next chapter we will turn to a complex frequency-domain representation of discrete-time systems, which does allow further analysis.

The main advantage digital IIR filters have over FIR filters is their efficiency in implementation, in order to meet a specification in terms of passband, stopband, ripple, and/or roll-off. Such a set of specifications can be accomplished with a lower order IIR filter than would be required for an FIR filter meeting the same requirements. If implemented in a signal processor, this implies a correspondingly fewer number of calculations per sample; the computational savings is often of a rather large factor.

IIR filters have a number of disavantages. They can be hard to design, for instance, to match a particular frequency response requirement. Also IIR filter designs are not necessarily stable. It is hard to achieve a linear phase IIR filter and then only as an approximation (for instance with the Bessel filter). Another issue regarding digital IIR filters is the potential for limit cycle behavior, due to the feedback system in conjunction with quantization. The latter aspect is however beyond the scope of the workshop.

\begin{exmp}
    \begin{figure}[ht]
        \centering
        \includegraphics[scale=1]{figures/DC_remover_block_diagram.eps}
        \caption{Blockdiagram of an IIR high-pass filter.} \label{fig:Blockdiagram_DC_remover}
    \end{figure}
    \begin{figure}[hb]
        \centering
        \includegraphics[width=0.6\linewidth]{figures/IR_DC_remover.eps}
        \caption{Impulse response of an IIR high-pass filter, with $a=0.5$.} \label{fig:IR_DC_remover}
    \end{figure}
    Let's have a look at an example depicted in Figure \ref{fig:Blockdiagram_DC_remover}, which is a first order high-pass filter (HPF). We can evaluate the first samples of its impulse response manually by applying a Kronecker delta to the input, as printed in the Table.
    \begin{table}[h!]
        \centering
        \begin{tabular}[hb]{l|l|l|}
            n & $x[n]=\delta[n]$ & $y[n]$ \\
            \hline
            0 & 1 & $1$ \\
            1 & 0 & $a-1$ \\
            2 & 0 & $a^2 - a$\\
            3 & $\cdots$ & $\cdots$
        \end{tabular}
        \caption{Sample values in an IIR HPF filter.}
    \end{table}
    Since we are dealing with an IIR filter we can continue this excercise forever. Using Matlab or another design tool we can have a PC compute the impulse response for a larger number samples. In Figure \ref{fig:IR_DC_remover} the result is depicted for $a=0.5$.
\end{exmp}



\section{Further reading}
% For more information on discrete time systems, see e.g. 
\begin{itemize}
    \item \url{http://en.wikipedia.org/wiki/Filter_(signal_processing)}
    \item \url{http://en.wikipedia.org/wiki/Digital_filter}
    \item \url{http://en.wikipedia.org/wiki/Linear_time-invariant_system}
    \item \url{http://en.wikipedia.org/wiki/Discrete_time_and_continuous_time}
    \item \url{http://en.wikipedia.org/wiki/Convolution}
    \item \url{http://en.wikipedia.org/wiki/Causal_system}
    \item \url{http://en.wikipedia.org/wiki/Infinite_discrete_convolution}
    \item \url{http://en.wikipedia.org/wiki/BIBO_stability}
    \item \url{http://en.wikipedia.org/wiki/Finite_impulse_response}
    \item \url{http://en.wikipedia.org/wiki/Infinite_impulse_response}
    \item \url{http://en.wikibooks.org/wiki/Digital_Signal_Processing}
\end{itemize}

