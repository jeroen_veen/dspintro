// N is the number of filter coefficients
// x and y are input and output, respectively
// coeffs[] contains the coefficients
// buf[] contains the circular buffer 
// ind is an iterator

buf[ind++] = x;
if (ind >= N)
  ind = 0; // reset the iterator

acc = 0.0; // accumulated value
for(i=0; i<N; i++) {
  acc += buf[ind++] * coeffs[N-i]; // add 
  if (ind >= N)
    ind = 0; // reset the iterator
}
y = acc;