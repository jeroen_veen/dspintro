// x and y are input and output, respectively
// b0, b1, b2, a1, and a2 are the filter coefficients. 
// a1 and a2 are negated. 
// w[] contains state variables
w[0] = x + a1*w[1] + a2*w[2]; 
y = b0*w[0] + b1*w[1] + b2*w[2];
// Persist state variables for next call
w[2] = w[1]; 
w[1] = w[0];