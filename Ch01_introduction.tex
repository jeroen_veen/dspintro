\chapter{The basics} \label{chap:basics}

\section{Introduction}

Digital signal processing (DSP) and analog signal processing are subfields of signal processing. DSP applications include audio and speech processing, sonar, radar and other sensor array processing, spectral density estimation, statistical signal processing, digital image processing, signal processing for telecommunications, control systems, biomedical engineering, seismology, among others. DSP is applied with a wide variety of goals, such as: enhancement of visual images, recognition and generation of speech, compression of data for storage and transmission, etc. DSP can involve linear or nonlinear operations, but in this workshop only linear operations will be discussed.
DSP gives many advantages over analog processing in many applications, such as error detection and correction in transmission as well as data compression. DSP is applicable to both streaming data and static (stored) data.
Nowadays, digital signal processing is at the heart of our everyday life as well as of fundamental scientific advances. For example, a smartphone is a concentrate of signal processing, operating many functions that allow us to communicate, exchange or store messages, music and pictures or videos.

\begin{table}[h!]
\centering
\begin{tabular}[b]{|l|l|}
    \hline
    Communication & Digital telephony, \\ 
    &Voice and data compression, \\
    &Echo suppression.\\ \hline
    Industrial & Process monitoring and control,\\
    &Non-destructive testing,\\
    &Oil prospecting.\\ \hline
    Entertainment & Multimedia compression and streaming,\\
    &Gaming user interfaces,\\
    &Video conferencing.\\ \hline
    Medical & Patient monitoring,\\
    &Diagnostic imaging (e.g. MRI, PET, ultra sound),\\
    &DNA analysis and comparison.\\ \hline
    Automotive & Radar and lidar,\\
    &Motor control,\\
    &Advanced Driver Assistance Systems.\\ \hline
    Science & Data acquisition,\\
    &Data analysis and mining,\\
    &Environmental monitoring.\\\hline
    Space & Radio telescope system\\
    & Hyperspectral imaging,\\
    & Satellite sensor analysis.\\ \hline
    Military & Radar and sonar,\\
    &Missile interception,\\
    &Sensory intelligence.\\ \hline
\end{tabular}
\caption{A few diverse DSP applications.} \label{tab:DSP_applications}
\end{table}

Let’s look at one application in a bit more detail: Telecommunications, i.e. the transmission of signs, signals, messages, words, writings, images and sounds or information of any nature by wire, radio, optical or electromagnetic systems. Telecommunication occurs when the exchange of information between communication participants includes the use of technology. It is transmitted either electrically over physical media, such as cables, or via electromagnetic radiation. Such transmission paths are often divided into communication channels which afford the advantages of multiplexing. Since the Latin term communicatio is considered the social process of information exchange, the term telecommunications is often used in its plural form because it involves many different technologies.
Early means of communicating over a distance included visual signals, such as beacons, smoke signals, semaphore telegraphs, signal flags, and optical heliographs. Other examples of pre-modern long-distance communication included audio messages such as coded drumbeats, lung-blown horns, and loud whistles. 20th- and 21st-century technologies for long-distance communication usually involve electrical and electromagnetic technologies, such as telegraph, telephone, and teleprinter, networks, radio, microwave transmission, fiber optics, and communications satellites.
Communications signals can be sent either by analog signals or digital signals. There are analog communication systems and digital communication systems. For an analog signal, the signal is varied continuously with respect to the information. In a digital signal, the information is encoded as a set of discrete values (for example, a set of ones and zeros). During the propagation and reception, the information contained in analog signals will inevitably be degraded by undesirable physical noise. (The output of a transmitter is noise-free for all practical purposes.) Commonly, the noise in a communication system can be expressed as adding or subtracting from the desirable signal in a completely random way. This form of noise is called additive noise, with the understanding that the noise can be negative or positive at different instants of time. Unless the additive noise disturbance exceeds a certain threshold, the information contained in digital signals will remain intact. Their resistance to noise represents a key advantage of digital signals over analog signals.
Today, there are more mobile phones than people in the world. Telecommunications in which multiple transmitters and multiple receivers have been designed to cooperate and to share the same physical channel are called multiplex systems. The sharing of physical channels using multiplexing often gives very large reductions in costs. Multiplexed systems are laid out in telecommunication networks, and the multiplexed signals are switched at nodes through to the correct destination terminal receiver. Until the 1960s, a connection between two telephones required passing the analog voice signals through mechanical switches and amplifiers. One connection required one pair of wires. In comparison, DSP converts audio signals into a stream of serial digital data. Since bits can be easily intertwined and later separated, many telephone conversations can be transmitted on a single channel. Next to multiplexing and improving the robustness of phone calls, DSP is also applied to compress data and perform audio enhancement processing, such as echo cancellation. For more info on the breadth and depth of DSP, please see the references at the end of the chapter.


\section{Digital signals}

In engineering, a signal can be defined as a function representing some variable that contains some information about the behavior of a natural or artificial system. Signals are meaningless without systems to interpret them, and systems are useless without signals to process. Before we can start building a system, we have to first think about acquiring digital signals.

To digitally analyze and manipulate an analog signal, or continuous-time (CT), it must be digitized with an analog-to-digital converter (ADC). Sampling is usually carried out in two stages, discretization and quantization. Discretization means that the signal is divided into equal intervals of time, and each interval is represented by a single measurement of amplitude. Quantization means each amplitude measurement is approximated by a value from a finite set. Rounding real numbers to integers is an example. So, an ADC converts a time-varying voltage signal into a discrete-time signal, a sequence of real numbers. Quantization replaces each real number with an approximation from a finite set of discrete values.
Theoretical DSP analyses and derivations are typically performed on discrete-time signal models with no amplitude inaccuracies (quantization error), "created" by the abstract process of sampling. Numerical methods require a quantized signal, such as those produced by an ADC. The processed result might be a frequency spectrum or a set of statistics. But often it is another quantized signal that is converted back to analog form by a digital-to-analog converter (DAC). 

\subsection{Signal sampling}
\begin{figure}[b!]
    \centering
    \includegraphics[width=0.6\linewidth]{figures/Signal_Sampling.png}
    \caption{Signal sampling representation. (\small{source: \href[]{en.wikipedia.org/wiki/File:Signal_Sampling.png}{Wikipedia}}).}{The continuous signal is represented with a green colored line while the discrete samples are indicated by the blue vertical lines} \label{fig:Signal_Sampling}
\end{figure}
Discrete time views values of variables as occurring at distinct, separate "points in time", or equivalently as being unchanged throughout each region of time ("time period")—that is, time is viewed as a discrete variable. Thus, a non-time variable jumps from one value to another as time moves from one time period to the next. This view of time corresponds to a digital clock that gives a fixed reading of 10:37 for a while, and then jumps to a new fixed reading of 10:38, etc. In this framework, each variable of interest is measured once at each time period. The number of measurements between any two time periods is finite. Measurements are typically made at sequential integer values of the variable "time". Note, that sampling can be done for functions varying in space, time, or any other dimension, and similar results are obtained in two or more dimensions.

So, let’s recapitulate:
\begin{itemize}
\item A sample is a value or set of values at a point in time and/or space.
\item A sampler is a subsystem or operation that extracts samples from a continuous signal.
\item A theoretical ideal sampler produces samples equivalent to the instantaneous value of the continuous signal at the desired points. 
\end{itemize}
Since a practical ADC cannot make an instantaneous conversion, the input value must necessarily be held constant during the time that the converter performs a conversion (called the conversion time). An input circuit called a sample and hold performs this task—in most cases by using a capacitor to store the analog voltage at the input and using an electronic switch or gate to disconnect the capacitor from the input. Many ADC integrated circuits include the sample and hold subsystem internally.
\begin{exmp}
    Suppose you are keeping a weather log by measuring the temperature in your hometown at noon and noting your observation in whole numbers in your agenda. The temperature log could be considered as a digital signal, since a) the real temperature is sampled periodically, and b) the real temperature is quantized with 1 degree Celcius step.
\end{exmp}

\subsection{Discrete-time signal representation}
\begin{figure}[t!]
    \centering
    \includegraphics[width=0.6\linewidth]{figures/stem.eps}
    \caption{Graphical representation of a time-discrete signal.} \label{fig:stem}
\end{figure}   
In this workshop, we will only consider discrete-time, i.e. sampled, signals. For functions that vary with time, let $s(t)$ be a continuous function (or "signal") to be sampled, and let sampling be performed by measuring the value of the continuous function every $T$ seconds, which is called the sampling interval or the sampling period. Then the sampled function is given by the sequence:
\begin{equation} \label{eq:sampling}
\begin{matrix}
s[n] = s(n T), &  n \in \mathbb{N} 
\end{matrix} ,
\end{equation}
where $n$ is the sample number, iteration number, or time period number. The sampling frequency or sampling rate, $f_s$, is the average number of samples obtained in one second (samples per second), thus $f_s = 1/T$.

\begin{exmp}
    Consider a continuous time signal 
    \begin{equation*}
        s(t) = A \cos{(2 \pi f_0 t + \phi)},
    \end{equation*}
    which represents a sinusoid with amplitude, $A$, frequency, $f_0$, and phase-shift $\phi$.
    Assuming that $f_0 <f_{nyq}$, the discrete-time function equals
    \begin{equation*}
        s[n] = A \cos{(2 \pi f_0 n T + \phi)}. 
    \end{equation*}
    Software packages like MATLAB, GNU Octave, Scilab, and Matplotlib provide convenient methods to plot time-discrete signals. For instance, using the MATLAB function \lstinline[language=matlab, style=Matlab-editor]!stem!, a nice graphical representation can be created. In Figure \ref{fig:stem}, an example is plotted, produced by the script of Listing \ref{code:stem}. Also, an approximation of a continuous time signal is plotted, which was created by choosing a really high sampling frequency.   
    Note that software mathematical frameworks do not use explicitly defined units, it is up to the user to interpret the results in the 
    \lstinputlisting[float, language=matlab, style=Matlab-editor, caption={Matlab script for producing a plot of a discrete-time signal.}, label=code:stem]{scripts/exmp_stem.m}
    proper time and amplitude.
\end{exmp}
In order to represent a discrete-time sample sequence in a formula form it is common to make use of the Kronecker delta function, $x[n]=\delta[n]$, where the Kronecker delta is defined as:
\begin{equation} \label{eq:kron_delta}
        \delta[n] = 
            \left \{ 
                \begin{array}{ll}
                    0, & \text{if } n \neq 0\\
                    1, & \text{if } n = 0
                \end{array}
            \right .
\end{equation}

\begin{exmp}
    % \begin{minipage} {\textwidth}
    \begin{figure}[t!] \label{fig:sequence}
        \centering
        \includegraphics[width=0.6\linewidth]{figures/sequence.eps}
        \caption{Graphical representation of a time-discrete signal.}
    \end{figure}        
    Consider a sequence of $4$ samples
    \begin{equation*}
        x[0] = .5, x[1] = 3.0, x[2] = -0.5, x[3] = 1.0 ,
    \end{equation*}
    which can be written in a formula form as:
    \begin{equation*}
        x[n] = .5 \delta[n] + 3.0 \delta[n-1] - 0.5 \delta[n-2] + \delta [n-3], 
    \end{equation*}   
    where successive samples are individually represented by introducing time-delays.  In Figure \ref{fig:sequence}, an example is plotted, produced by the script of Listing \ref{code:sequence}. Also, an approximation of a continuous time signal is plotted, which was created by choosing a really high sampling frequency. 
    \lstinputlisting[language=matlab, style=Matlab-editor, caption={Matlab script for producing a plot of a discrete-time signal.}, label=code:sequence]{scripts/exmp_sequence.m}
    % \end{minipage}
\end{exmp}


\subsection{Quantization}
\begin{figure}[t!]
    \centering
    \includegraphics[width=0.4\linewidth]{figures/3-bit_resolution_analog_comparison.png}
    \caption{A continuous sinusoid being quantized with 3-bit resolution (\small{source: \href[]{en.wikipedia.org/wiki/Quantization_(signal_processing)}{Wikipedia}}).} \label{fig:quantization}
\end{figure}

Quantization, in mathematics and digital signal processing, is the process of mapping input values from a large set (often a continuous set) to output values in a (countable) smaller set, often with a finite number of elements. Rounding and truncation are typical examples of quantization processes. Quantization is involved to some degree in nearly all digital signal processing, as the process of representing a signal in digital form ordinarily involves rounding. Quantization also forms the core of essentially all lossy compression algorithms. 

The resolution of a quantizer indicates the number of discrete values it can produce over a range of analog values. The resolution determines the magnitude of the quantization error. The values are usually stored electronically in binary form, in consequence, the number of discrete values available is assumed to be a power of two. For example, an ADC with a resolution of 8 bits can encode an analog input to one in 256 different levels ($2^8 = 256$). The values can represent the ranges from 0 to 255 (i.e. unsigned integer) or from -128 to 127 (i.e. signed integer), depending on the application. 

Resolution can also be defined electrically, and expressed in volts. The change in voltage required to guarantee a change in the output code level is called the least significant bit (LSB) voltage. The resolution Q of the ADC is equal to the LSB voltage. The voltage resolution of an ADC is equal to its overall voltage measurement range divided by the number of intervals:
\begin{equation*}
    Q = \frac {V_{FSR}} {2^M},
\end{equation*}
where $M$ is the ADC resolution, and $V_{FSR}$ is the full scale voltage range (also called 'span'). 

\begin{exmp}
    Figure \ref{fig:ADC_coding} shows an example of a possible coding scheme with a full scale range from 0 to 1 V ($E_{FSR}$ in this case. ) The ADC resolution is $M=3$ bits, so $N = 2^3 = 8$ quantization levels (codes), and the ADC voltage resolution equals: $Q = 1 V / 8 = 0.125 V$.
    \begin{figure}[t!]
        \centering
        \includegraphics[width=0.5\linewidth]{figures/ADC_voltage_resolution.png}
        \caption{3-bit resolution results in an 8-level ADC coding scheme (\small{source: \href[]{en.wikipedia.org/wiki/Analog-to-digital_converter}{Wikipedia}}).} \label{fig:ADC_coding}
    \end{figure} 
\end{exmp}

The difference between an input value and its quantized value (such as round-off error) is referred to as quantization error. A device or algorithmic function that performs quantization is called a quantizer, and an analog-to-digital converter is an example of a quantizer. Although quantization can be an important topic in DSP, we will not go into further detail in this workshop, and we will only consider discrete-time, i.e. sampled signals. Figure \ref{fig:quantization} depicts an example of a coded sinusoid, from which the introduction of quantization error becomes clear.


\subsection{Aliasing}
Aliasing is a common problem in digital processing applications. Many readers have heard of "anti-aliasing" features in high-quality video cards. Aliasing is an effect of violating the Nyquist-Shannon sampling theorem. In practice, an ADC works by sampling the value of the input at discrete intervals in time. Provided that the input is sampled above the Nyquist rate, defined as twice the highest frequency of interest, then all frequencies in the signal can be reconstructed. So, 
\begin{equation} \label{eq:Nyquist}
    f_{nyq} = f_s / 2 .
\end{equation}
If frequencies above half the Nyquist rate are sampled, they are incorrectly detected as lower frequencies, a process referred to as aliasing. Aliasing occurs because instantaneously sampling a function at two or fewer times per cycle results in missed cycles, and therefore the appearance of an incorrectly lower frequency. For example, a 2 kHz sine wave being sampled at 1.5 kHz would be reconstructed as a 500 Hz sine wave.

\begin{exmp}
    Sinusoids are an important type of periodic functions, because realistic signals are often modeled as the summation of many sinusoids of different frequencies and different amplitudes (for example, with a Fourier series or transform). Understanding what aliasing does to the individual sinusoids is useful in understanding what happens to their sum.
    \begin{figure}[ht]
        \centering
        \includegraphics[width=0.6\linewidth]{figures/1920px-AliasingSines.png}
        \caption{Aliasing: two different sinusoids that fit the same set of samples. (\small{source: \href[]{en.wikipedia.org/wiki/Aliasing}{Wikipedia}}).} \label{fig:aliasing}
    \end{figure}

    Consider Figure \ref{fig:aliasing}, where a plot depicts a set of samples whose sample-interval is 1, and two (of many) different sinusoids that could have produced the samples. The sample-rate in this case is $f_s = 1$. Nine cycles of the red sinusoid and one cycle of the blue sinusoid span an interval of ten samples. The corresponding number of cycles per sample are $f_{red} = 0.9$ and $f_{blue} = 0.1$. Therefore it is unclear, if these samples were produced by sampling functions $\cos{(2 \pi 0.9 t - \theta)}$ and $\cos{(2 \pi 0.1 t - \theta)}$. 
\end{exmp}

Aliasing matters when one attempts to reconstruct the original waveform from its samples. The most common reconstruction technique produces the smallest of the frequencies possible. The Nyquist–Shannon sampling theorem states that a signal can be exactly reconstructed from its samples if the sampling frequency is greater than twice the highest frequency component in the signal. In practice, the sampling frequency is often significantly higher than twice the Nyquist frequency. The easiest way to prevent aliasing is the application of a steep sloped low-pass filter with half the sampling frequency before the conversion. To avoid aliasing, the input to an ADC must be low-pass filtered to remove frequencies above half the sampling rate. This filter is called an anti-aliasing filter (AAF), and is essential for a practical ADC system that is applied to analog signals with higher frequency content. In applications where protection against aliasing is essential, oversampling may be used to greatly reduce or even eliminate it. 


\section{Digital signal processing systems}
DSP algorithms may be run on general-purpose computers and digital signal processors. When the application requirement is real-time, DSP is often implemented using specialized or dedicated processors or microprocessors, sometimes using multiple processors or multiple processing cores. DSP algorithms are also implemented on purpose-built hardware such as application-specific integrated circuit (ASICs). Additional technologies for digital signal processing include more powerful general-purpose microprocessors, field-programmable gate arrays (FPGAs), digital signal controllers (mostly for industrial applications such as motor control), and stream processors. In general, a DSP front-end can schematically be depicted as a sequence of an analog anti-aliasing filter (AAF) and an analog-to-digital converter (ADC), followed by the digital signal processor (DSP) itself.\begin{figure}[ht]
    \centering    
    \includegraphics[scale=1.0]{figures/DSP_system.eps}
    \caption{Blockdiagram of the complete DSP system.} \label{fig:DSP_system}
\end{figure}  
Note that when the processing requirement is not real-time and the signal data (either input or output) exists in data files, processing is economically done with an existing general-purpose computer. This is essentially no different from any other data processing, except DSP mathematical are used, and the sampled data is usually assumed to be uniformly sampled in time or space. For example: processing digital photographs with software such as Photoshop, or editing and producing audio files with a digital audio workstation (DAW) such as Cubase.

\section{Further reading}
For more information on digital signal processing and its applications, see e.g. 
\begin{itemize}
    \item \url{http://en.wikipedia.org/wiki/Digital_signal_processing}
    \item \url{http://en.wikipedia.org/wiki/Analog-to-digital_converter}
    \item \url{http://en.wikibooks.org/wiki/Digital_Signal_Processing}    
    \item \url{http://en.wikipedia.org/wiki/Quantization_(signal_processing)}
    \item \url{http://en.wikipedia.org/wiki/Telecommunication}
    \item \url{http://en.wikipedia.org/wiki/Nyquist%E2%80%93Shannon_sampling_theorem}
\end{itemize}

% \begin{itemize}
%     \item \url{http://en.wikipedia.org/wiki/Digital_signal_processing}
%     \item \url{http://en.wikipedia.org/wiki/Analog-to-digital_converter}
%     \item \url{http://en.wikipedia.org/wiki/Quantization_(signal_processing)}\item \url{http://en.wikipedia.org/wiki/Telecommunication}
%     \item \url{http://en.wikipedia.org/wiki/Filter_(signal_processing)}
%     \item \url{http://en.wikipedia.org/wiki/Digital_filter}
%     \item \url{http://en.wikipedia.org/wiki/Digital_signal_processing}
%     \item \url{http://en.wikipedia.org/wiki/Discrete_time_and_continuous_time}
%     \item \url{http://en.wikipedia.org/wiki/Summation}
%     \item \url{http://en.wikipedia.org/wiki/Linear_time-invariant_system}
%     \item \url{http://en.wikipedia.org/wiki/Infinite_discrete_convolution}
%     \item \url{http://en.wikipedia.org/wiki/Z-transform}
%     \item \url{http://en.wikipedia.org/wiki/Bilinear_transform}
% \end{itemize}
