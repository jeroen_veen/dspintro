numz = [1,0,-1]
denz = [2,2,1]
[z,p,k] = tf2zpk(numz, denz)

figure; hold on; grid on; box on;
impz(numz,denz)

figure; hold on; grid on; box on;
zplane(z,p);
axis([-1 1 -1 1])