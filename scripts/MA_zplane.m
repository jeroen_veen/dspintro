% Set MA filter order
P = 2;

% Define the numerator and denominator polynomials
numz = ones(1,P+1)/(P+1);
denz = 1;

% Compute poles and zeroes
[z,p,k] = tf2zpk(numz, denz);

figure; hold on; grid on; box on;
zplane(z,p);
axis([-1 1 -1 1]);
