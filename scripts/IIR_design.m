Fp = 100;
Fst = 300;
Ap = 1;
Ast = 60;
Fs = 2e3;
method = 'butter';

dbutter = designfilt('lowpassiir','PassbandFrequency',Fp,...
    'StopbandFrequency',Fst,'PassbandRipple',Ap,...
    'StopbandAttenuation',Ast,'SampleRate',Fs,'DesignMethod',method);

fvtool(dbutter)

