fs = 1e3; % sample frequency [Hz] [Sps]
fnyq = fs/2; % Nyquist frequency [Hz]

fc = 200; % RC cut-off frequency [Hz]


wc = 2*pi*fc; % cut-off frequency [rad/s]
Ts = 1/fs; % sample period [s]

wc_acc = (2/Ts) * tan(wc*Ts/2);

abs(wc-wc_acc)/(2*pi)


% 
% 
% wc = wc + (wc-wc_acc);
% wc_acc = (2/Ts) * tan(wc*Ts/2);
% wc_acc/(2*pi)
