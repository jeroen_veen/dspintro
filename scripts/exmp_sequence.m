x = [0.5, 3.0, -0.5, 1.0]; % A sequence of samples
n = [0:length(x)-1];  % The sample numbers

figure; hold on; grid on; box on;  % figure with bounding box and grid
stem(n,x,'k');
xlabel('n[#]');
ylabel('x[a.u.]');
axis([-1 5 -1 4])
