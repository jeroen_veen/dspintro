Fp = 370;
Fst = 430;
Ap = 1;
Ast = 30;
Fs = 2e3;

d = designfilt('lowpassfir','PassbandFrequency',Fp,...
    'StopbandFrequency',Fst,'PassbandRipple',Ap,...
    'StopbandAttenuation',Ast,'SampleRate',Fs,'DesignMethod','equiripple');

fvtool(d)