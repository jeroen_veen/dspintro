fs = 20e3;
fc = 3e3;
N = 41;

T = 1/fs;
fN = fs/2;
n = [-floor(N/2):floor(N/2)]';
numz = sin(n*pi*fc/fN)./(n*pi);
numz(round(length(n)/2)) = fc/fN;
denz = 1;

figure; hold on; grid on; box on;
impz(numz,denz);

figure; hold on; grid on; box on;
[h,f] = freqz(numz,denz,1e5,fs);
plot(f*1e-3, 20*log10(h))
xlabel('f [kHz]');
ylabel('|H| [dB]');
axis([0 fN*1e-3 -50 5]);

