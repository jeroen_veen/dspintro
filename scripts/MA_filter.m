P = 2 % MA filter order

% define 
numz = ones(1,P+1)/(P+1)
denz = 1

[z,p,k] = tf2zpk(numz, denz)
% H = tf(numz,denz,1)

x = sin(2*pi*[0:49]/30) +.5*randn(1,50);
y = filter(numz,denz,x);

figure; hold on; grid on; box on;
stem(x,'b','filled')
stem(y,'r','filled')
xlabel('n (samples)') 
ylabel('Amplitude')
legend('x', 'y')
crap;
figure; hold on; grid on; box on;
impz(numz,denz)
axis([0 3 -.5 .5 ])

figure; hold on; grid on; box on;
zplane(z,p);
axis([-1 1 -1 1])

figure; hold on; grid on; box on;
freqz(numz,denz)


P = [2,4,8];    % order
K=200;   % number of datapoints

theta=[-pi:2*pi/K:pi]; % frequency scale

abs_H = abs(sin(kron(P,theta')/2)./(kron(P,sin(theta'/2))));

figure; hold on; grid on; box on;
plot(theta/(pi), abs_H);
legend('P=2', 'P=4', 'P=8');
axis([-1 1 0 1]);
xlabel('f/f_{nyq}'); ylabel('|H|');
