f_s = 10e3;  % Sample frequency [Hz]

f_0 = 1e3;  % Signal frequency [Hz]
A = 2;  % Signal amplitude [a.u.]

N = 11;  % Total number of samples to be displayed [#]
n = [0:N-1];  % A vector with N sample numbers
s = A*sin(2*pi*f_0*n/f_s);  % The signal

figure; hold on; grid on; box on;  % figure with bounding box and grid
plot([0:.01:N-1],A*sin(2*pi*(f_0/f_s)*[0:.01:N-1]),'b:');
stem(n,s,'k');
xlabel('n[#]');
ylabel('A[a.u.]');
legend('continuous', 'discrete-time')
