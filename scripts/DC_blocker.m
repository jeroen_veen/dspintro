% DC blocker example
a = .5
numz = [1,-1,0]
denz = [1,-a]
[z,p,k] = tf2zpk(numz, denz)

figure; hold on; grid on; box on;
impz(numz,denz)

figure; hold on; grid on; box on;
zplane(z,p);
axis([-1 1 -1 1])
