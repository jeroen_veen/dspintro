fs = 1000;            % Sampling frequency                    
T = 1/fs;             % Sampling period       
L = 1500;             % Length of signal
t = (0:L-1)*T;        % Time vector

% Form a signal containing a 50 Hz sinusoid of amplitude 0.7 
% and a 120 Hz sinusoid of amplitude 1.
s = 0.7*sin(2*pi*50*t) + sin(2*pi*120*t);

% Corrupt the signal with zero-mean white noise with a variance of 4.
x = s + 2*randn(size(t));

% Plot the noisy signal in the time domain. 
figure; hold on; grid on; box on;
plot(1000*t(1:50), x(1:50));
title('Signal Corrupted with Zero-Mean Random Noise');
xlabel('t (milliseconds)');
ylabel('x(t)');

% Compute the Fourier transform of the signal. 
Y = fft(x);
P2 = abs(Y/L);  % Compute the two-sided spectrum P2.
P1 = P2(1:L/2+1);  % Compute the single-sided spectrum P1 based on P2 
P1(2:end-1) = 2*P1(2:end-1); % correct the amplitude

f = fs*(0:(L/2))/L; % nonnegative frequency axis

figure; hold on; grid on; box on;
plot(f,P1) 
title('Single-Sided Amplitude Spectrum of x(t)')
xlabel('f (Hz)')
ylabel('|P1(f)|')
