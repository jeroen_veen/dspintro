\chapter{The frequency domain} \label{chap:frequency_domain}

\section{Introduction}

In DSP, engineers usually study digital signals in one of the following domains: time domain (one-dimensional signals), spatial domain (multidimensional signals), frequency domain, and wavelet domains. They choose the domain in which to process a signal by making an informed assumption (or by trying different possibilities) as to which domain best represents the essential characteristics of the signal and the processing to be applied to it. 

The most common purpose for analysis of signals in the frequency domain is analysis of signal properties. The engineer can study the spectrum to determine which frequencies are present in the input signal and which are missing. Frequency domain analysis is also called spectrum- or spectral analysis.

A sequence of samples from a measuring device produces a temporal or spatial domain representation, whereas a discrete Fourier transform produces the frequency domain representation. Signals are converted from time or space domain to the frequency domain usually through use of the Fourier transform. The Fourier transform converts the time or space information to a magnitude and phase component of each frequency. With some applications, how the phase varies with frequency can be a significant consideration. Where phase is unimportant, often the Fourier transform is converted to the power spectrum, which is the magnitude of each frequency component squared.



\section{From the z-domain to the frequency domain}

With the introduction of the z-transform in Equation \ref{eq:z_transform}, the complex variable $z$ was defined:
\begin{align*} 
    z 
    & = A e^{j \phi} \\
    & = A \left ( \cos(\phi) + j \sin(\phi) \right ) \nonumber ,
\end{align*}
where $|z| = A$ is the magnitude of $z$, and $\phi$ is the angle or phase. Loosely speaking, the variable $z$ can be understood as a complex frequency.
For values of $z=e^{j \phi}$ on the unit circle (with $\vert z \vert = 1$), we can express the z-transform as a function of a single, real variable $\omega$ [rad/s], by first defining 
\begin{equation}
z = e^{j \omega T} ,
\end{equation}
and rewriting the bilateral z-transform as: 
\begin{equation} \label{eq:DTFT_transform}
    X(z) = \sum_{n=-\infty}^\infty x[n] z^{-n} = \sum_{n=-\infty}^\infty x[n] e^{-j \omega n T} = X(e^{j \omega T})  , 
\end{equation}
which is also known as the {\bf discrete-time Fourier transform (DTFT)} of $x[n]$. This $2 \pi$-periodic function is the periodic summation of a standard Fourier transform, which makes it a widely used analysis tool in Engineering. Furtheron in this chapter, the family of Fourier transforms is discussed in more detail.
\begin{figure}[ht]
    \centering
    \includegraphics[scale=1]{figures/z_plane_freq.eps}
    \caption{Frequencies in the z-plane.} \label{fig:z_plane_freq} 
\end{figure}
The frequency response of a system can be computed by either evaluating the transfer function $H(z)$ on the unit circle in the pole-zero diagram, or by substituting $z = e^{j \omega T} $ in the formula of $H(z)$. Before we will actually begin deriving frequency responses, first let's introduce the very useful concept of negative and positive frequency. {\bf Negative frequency} is a mathematical construct that can be interpreted as a wheel rotating one way or the other way: a signed value of frequency can indicate both the rate and direction of rotation. The rate is expressed in units such as revolutions (a.k.a. cycles) per second (Hertz) or radian/second (where 1 cycle corresponds to $2 \pi$ radians). For example, if a sequence $s[n]$ of samples is obtained by sampling a function $s(t) = \cos{(2 \pi 0.5 t - \theta)}$, then this sequence $s[n]$ could also have been produced by the trigonometrically identical function $\cos{(2 \pi (-0.5) t - \theta)}$. In the z-plane we can also define positive and negative frequencies; rotating in counterclockwise results in positive frequencies and clockwise in negative frequencies as sketched in Figure \ref{fig:z_plane_freq}. So starting from $z=1$, which is the point on the unit circle where $\phi=0 \implies \omega=0$, so DC, we rotate counterclockwise along the unit circle and $\omega$ increases. Now, when the angle reaches $\omega T=\pi$, we can derive the corresponding frequency:
\begin{equation}
    z=-1 \implies f = \frac{\omega} {2 \pi} = \frac{\pi/T} {2 \pi} = \frac{1} {2T} =  \frac{f_s} {2} ,
\end{equation}
i.e. the Nyquist frequency, $f_{nyq}$, as also indicated in Figure \ref{fig:z_plane_freq}. Now, when we rotate further in the counterclockwise direction, the corresponding frequency increase above the Nyquist frequency, which makes no sense in a discrete-time system. Finally, for $\phi = 2 \pi$, the corresponding frequency equals $f=f_s$ equivalent to $f=0$, and we are back at DC.
Rotating from $\omega = 0$ in the clockwise direction, we consider a negative angle $\phi$ and thus a negative $\omega$. Again for an angle $\phi = -\pi$ we and up at a freqency of $-f_{nyq}$. 
\begin{table}[h!]
    \centering
    \begin{tabular}{|c|c|c|c|}
        \hline
        $z$ & $\phi[\si{\radian}]$ & $\omega[\si{\radian/s}]$ & $f[\si{\Hz}]$ \\
        \hline
        1 & 0 & 0 & 0 \\
        \hline
        j & $\frac{\pi}{2}$ & $\frac{\pi}{2 T}$ & $\frac {f_s} {4}$ \\
        \hline
        -1 & $\pi$ & $\frac {\pi} {T}$ & $\frac {f_s} {2} = f_{nyq}$ \\
        \hline
        -j & $\frac{3 \pi}{2}$ = $- \frac{\pi} {2}$ & $\frac{3 \pi}{2 T}$ = $-\frac{\pi}{2 T}$ & $\frac {3 f_s} {4}$ = $-\frac{f_s} {4}$\\
        \hline
        1 & $ 2 \pi$ & $\frac{2 \pi} {T}$ & $f_s$ \\
        $\vdots$ & $\vdots$ & $\vdots$ & $\vdots$ \\
    \end{tabular}
    \caption{Rotating counterclockwise over the unit circle.} \label{tab:z_vs_omega}    
\end{table}
So when a frequency above $f_{nyq}$ is considered, it is effectively mapped to a lower frequency. This effect can also be illustrated by drawing a graph of the magnitude of the frequency response or spectrum, as depicted in Figure \ref{fig:spectrum_interval}. During sampling the base band spectrum of the sampled signal is mirrored to every multifold of the sampling frequency. These mirrored spectra are called alias or replicas.
\begin{figure}[ht]
    \centering
    \includegraphics[scale=1]{figures/spectrum_aliases.eps}
    \caption{Example of a signal amplitude spectrum on the fundamental interval (solid lines) and the replicas (dotted lines) of that same spectrum if higher frequencies are considered.} \label{fig:spectrum_interval}
\end{figure}

To summarize, Table  \ref{tab:z_vs_omega} shows a summary of the variables and values involved when rotating counterclockwise over the unit circle.
In practice, we will consider only the {\bf fundamental interval} or principal interval, which limits rotation to the range $-\pi \le \phi \le \pi$, or equivalently $-\pi/T \le \omega \le \pi/T$, and $-f_{nyq} \le f \le f_{nyq}$.



\section{Frequency responses}

When considering a transfer function, $H(z)$, the frequency response can be found by either evaluating $H(z)$ on the unit circle in the pole-zero diagram, or by directly substituting $z = e^{j \omega T}$ in the formula of $H(z)$. We will not dive much into the complete process to determine the frequency response directly from the z-plane. It is, however, useful to obtain some basic insights. For each point on the unit circle, i.e. and thus each frequency, the influence of each pole and zero on the system should be addressed. Obviously, poles lead to resonances and zeroes result in damping, therefore the magnitude of the frequency reponse $\vert H(e^{-j \omega T}) \vert$ can be found by weighing their respective contributions: 
\begin{equation} \label{eq:magnitude_from_zp_plot}
    \vert H(e^{-j \omega_e T}) \vert = K \frac {\Pi \text{ zero distances}} {\Pi \text{ pole distances}},
\end{equation}
where $\omega_e$ is the exact frequency considered, and thus defines a point on the unit circle. Furthermore, $K$ is the gain factor of the system, $\Pi$ denotes the product of multiple terms, and de distances indicate the  geometrical distance form a zero or pole to the point on the unit circle defined by $\omega_e$. In principle, one could obtain the complete frequency repsonse on the fundamental interval $-f_{nyq} \le f \le f_{nyq}$, however, such a tedious work is often omitted. Insted, it can be useful to determine the magnitude at some points on the interval, e.g. for the lowest frequency, DC, the highest frequency, $f_{nyq}$, and somewhere in between. In this way one can get quickly get a notion of the overall response of the filter; are we dealing with a low-pass or a high-pass filter, for example. 

Note that a similar reasoning applies to the computation of the phase of the frequency response at a particular frequency, but this procedure is omitted in this workshop.
\begin{exmp} 
    As an example let's return to the first-order IIR filter of Equation \ref{eq:TF_DC_blocker}, of which the pole-zero plot is repeated in Figure \ref{fig:z_plot_DC_remover_pz_distance} with $a=.5$.
    The zero, pole, and gain of $H(z)$ are given by:
    \begin{align*}
        z_{1} & = 1 \\
        p_{1} & = a = .5\\
        K & = 1 .
    \end{align*}
    Suppose we want to compute the magnitude of the frequency response at $\omega_e = \pi/ 2T$, i.e. $f=f_{nyq}/2$. For convenience, the zero and pole distances involved are indicated as dashed lines in Figure \ref{fig:z_plot_DC_remover_pz_distance}. The magnitude of the frequency reponse can be computed using Equation \ref{eq:magnitude_from_zp_plot}: 
    \begin{equation*}
        \vert H(e^{-j \omega_e T}) \vert = K \frac {\text{ zero distance}} {\text{ pole distance}} = \frac {\sqrt{2}} {\sqrt{1+.5^2}} \approx 1.26.
    \end{equation*}
    \begin{figure}[ht]
        \centering
        \includegraphics[width=0.6\linewidth]{figures/z_plot_DC_remover_pz_distance.eps}
        \caption{Pole-zero plot of a first-order IIR filter.} \label{fig:z_plot_DC_remover_pz_distance}
    \end{figure}
\end{exmp}
So, now we have seen a method to quickly determine the gain or damping of a system at a specific frequency. For recursive filters, i.e. IIR filters, it is often very useful to quickly find out at which frequencies the system resonates. We can again consider the pole-zero plot to find out the resonance frequencies. First, remember that when the impulse of the system is real, the upper and lower halves of the z-plane are mirror images of each other. This symmetry implies that we will only need to compute the frequency of the pole in the upper half-plane of the polse-zero plot, since the frequency of the mirrored pole will be the negated value. In order to determine a resonance frequency, we consider the angle between the positive real axis and a guide line from the origin to the pole location equal. In the previous example, this exercise is trivial, i.e. the pole is located at DC.
\begin{exmp}
    Consider the pole-zero plot of a discrete-time system $H(z)$ depicted in Figure \ref{fig:z_plot_exmp1}. We are asked to compute the system's resonance frequency, in case the system runs at a sampling frequency of $f_s = 1 kHz$. Note that also the unit circle $z=e^{j \phi}$ is depicted, which is the circle of radius one, centered at the origin. 
    \begin{figure}[ht]
        \centering
        \includegraphics[width=0.6\linewidth]{figures/IIR3_ZP_plot.eps}
        \caption{Pole-zero plot of a discrete-time system $H(z)$.} \label{fig:z_plot_exmp1}
    \end{figure}
    Since the function $H(z)$ has mirror symmetry in the real axis of the z-plane, we will only consider the pole in the upper half-plane. The angle between the positive real axis and a guide line from the origin to the pole location equals:
    \begin{equation*}
        \phi_r = \ang{45} = \frac {\pi} {4} \si{\radian},
    \end{equation*}
    and with $\phi = \omega T$, $\omega = 2 \pi f$, and $f_s=1/T$, it follows that the system resonates at 
    \begin{equation*}
        f_r = \frac {\omega_r} {2 \pi} = \frac {\phi_r} {2 \pi T} = \frac {\phi_r f_s} {2 \pi} = \frac {\pi} {4} \frac {f_s} {2 \pi} = \frac {f_s} {8} = 125 \text{ Hz}.
    \end{equation*}
    Obviosuly, the mirrored pole is the negated value, $-125$ Hz.
\end{exmp}
Finally, we will consider a method to determine the entire freqency response by substituting $z=e^{j\omega T}$ in the definition of a transfer function $H(z)$. Please realize that this method requires some mathematical manipulation, and there is no generic recipe available. It is best to just have a look at an example.
\begin{exmp}
    Again, let's look at the example of the second order moving average filter, depicted in Figure \ref{fig:Blockdiagram_z_MA_filter}. We already know the transfer function of the system (see Equation \ref{eq:TF_MA}), which is repeated here for convenience:
    \begin{equation*}
        H(z) = \frac{1}{3} \left (1 + z^{-1} + z^{-2} \right ) .
    \end{equation*}
   The frequency response of the MA filter can be found by substituting $z=e^{j\omega T}$ in the equation, 
    \begin{equation*}
        H(e^{j\omega T}) = \frac{1}{3} \left (1 + e^{-j\omega T} + e^{- 2 j\omega T} \right ) .
    \end{equation*}
    As such this expression does not directly provide much insight, and we will attempt to reformulate the frequency response. First, it is recognized that the addition of terms can be written more concise. In general, we can compute the sum of the first $n$ terms of a geometric series, i.e.
    \begin{equation*}
        \sum_{k=0}^{n-1} r^k = \frac {1-r^k} {1-r} .
    \end{equation*}
    Now, let $r=e^{-j \omega T}$, then:
    \begin{align*}
        H(e^{j\omega T}) 
        & = \frac{1}{3} \sum_{k=0}^2 e^{-j k \omega T} 
        = \frac{1}{3} \frac {1-e^{-j \omega 3 T}} {1-e^{-j \omega T}} \\
        & = \frac{1}{3} \frac{e^{-j \omega 3 T/2}} {e^{-j \omega T/2}} \left ( \frac {e^{-j k \omega 3 T/2} -e^{-j k \omega 3 T/2}} {e^{-j k \omega 3 T/2}-e^{-j k \omega T/2}} \right )\\
        & = \frac{1}{3} e^{-j \omega T} \left ( \frac {\sin{(3 \omega T/2)}} {\sin{(\omega T/2)}} \right ),
    \end{align*}
    where the trigoniometric identity $\sin{(\theta)} = \frac{1}{2j} \left (e^{+j\theta} - e^{-j\theta} \right)$ is used obtain the more compact form in the final line. For an MA filter, the derivation can be generalized, such that for arbitrary order $P-1$, the MA frequency response can be expressed as:
    \begin{equation} \label{eq:MA_freq_response}
        H(e^{j\omega T}) = \frac{1}{P} e^{-j \omega (P-1)T/2} \left ( \frac {\sin{(P \omega T/2)}} {\sin{(\omega T/2)}} \right ).
    \end{equation}
    Let's have a look at the magnitude of the complex function $H(e^{j\omega T})$, which is referred to as the amplitude response:
    \begin{equation} \label{eq:MA_amp_response}
        \vert H(e^{j\omega T}) \vert = \frac{1}{P} \left \vert \frac {\sin{(P \omega T/2)}} {\sin{(\omega T/2)}} \right \vert.
    \end{equation}
    What does this function tell us? Let's first find out where the zeroes of the amplitude response reside. What frequencies is the filter going to damp? Equating Expression \ref{eq:MA_amp_response} to zero implies the following:
    \begin{equation*}
        \vert H(e^{j\omega T}) \vert = 0 \implies \sin{(P \omega_0 T/2)} = 0 \implies P \omega_0 T/2 = \pi k, k \in \mathbb{Z} .
    \end{equation*}
    So, the set of zeroes can be expressed as:
    \begin{equation*}
        f_0(k) = \frac {\omega_0} {2 \pi} = \frac {2 \pi k} {2 \pi P T} = \frac {k} {P T}  = \frac {k f_s} {P} , k \in \mathbb{Z} .
    \end{equation*}
    On the fundamental interval $-f_{nyq} \le f \le f_{nyq}$ we will find $P-1$ zeroes, so equal to the filter order. Something funny happens when both the numerator and denominator of Equation \ref{eq:MA_amp_response} approach zero. Here, the above expression does not hold, we have a pole and a zero at the same frequency. We need to apply L'Hopital's rule to find a solution, i.e.
    \begin{equation*}
        \lim_{x \to c} \frac {f(x)} {g(x)} = \lim_{x \to c} \frac {f'(x)} {g'(x)} . 
    \end{equation*}
    On the fundamental interval this situation occurs once, for $\omega=0$, so:
    \begin{equation*}
        \lim_{\omega \to 0} \frac{1}{P} \frac {\sin{(P \omega T/2)}} {\sin{(\omega T/2)}} = \lim_{\omega \to 0} \frac{1}{P} \frac {\cos{(P \omega T/2)}PT/2} {\cos{(\omega T/2)}T/2} = \lim_{\omega \to 0}  \frac {\cos{(P \omega T/2)}} {\cos{(\omega T/2)}} = 1
    \end{equation*}
    From these considerations we can conclude that the MA filter will pass lower frequencies ($\omega \to 0$) and shows high damping at $f_0(k) = \frac {k f_s} {P} \le f_{nyq}$. Evaluating the amplitude response of the generalized MA filter for varying order $P$, shows that the MA response  indeed has low-pass characteristics (see Figure \ref{fig:MA_ampl_response}), although the filter stopband ripple is quite large.
    \begin{figure}[t]
        \centering
        \includegraphics[width=0.6\linewidth]{figures/MA_ampl_response.eps}
        \caption{MA filter amplitude response vs normalized frequency for various filter orders.} \label{fig:MA_ampl_response}
    \end{figure}
\end{exmp}

\begin{exmp}
    As an alternative to analytical derivation, software packages like MATLAB, GNU Octave, Scilab, and SciPy provide convenient methods to compute the frequency response. For instance, using the MATLAB function \lstinline[language=matlab, style=Matlab-editor]!freqz! the frequency response can be computed and plotted. Consider for example, a more complicated transfer function of a second-order IIR filter, given by
    \begin{equation*}
        H(z) = \frac{z^2-1}{2z^2+2z+1} .
    \end{equation*}
    The Bode plot of this IIR filter example is plotted in Figure \ref{fig:IIR_freqz_plot} and the Matlab script for producing the plot is printed in Listing \ref{code:IIR3_zplane}. 
    \begin{figure}[t]
        \centering
        \includegraphics[width=0.6\linewidth]{figures/IIR_freqz.eps}
        \caption{Bode diagram of a second order IIR filter.} \label{fig:IIR_freqz_plot}
    \end{figure}
    \lstinputlisting[float=b, language=matlab, style=Matlab-editor, caption={Matlab script for producing a frequency plot of an IIR filter.}, label=code:IIR3_zplane]{scripts/IIR_example3.m}
\end{exmp}


\section{Aliasing revisited}

In signal processing and related disciplines, aliasing is an effect that causes different signals to become indistinguishable (or aliases of one another) when sampled. It also refers to the distortion or artifact that results when the signal reconstructed from samples is different from the original continuous signal. Aliasing can occur in signals sampled in time, for instance digital audio, and is referred to as temporal aliasing. Aliasing can also occur in spatially sampled signals, for instance moiré patterns in digital images, and is called spatial aliasing in that case. 

Recall Equation \ref{eq:Nyquist}, the Nyquist–Shannon sampling theorem states that a signal can be exactly reconstructed from its samples if the sampling frequency is greater than twice the highest frequency component in the signal. So, 
\begin{equation}
    f_{nyq} = f_s / 2 .
\end{equation}
Typically, an anti-aliasing filter is used to remove components above the Nyquist frequency prior to sampling. Aliasing is generally avoided by applying low pass filters or anti-aliasing filters (AAF) to the input signal before sampling. Suitable reconstruction filters should then be used when restoring the sampled signal to the continuous domain. 
In practice, the sampling frequency is often significantly higher than twice the maximum frequency anticipated. Oversampling allows the application of low-order analog anti-aliasing filters, and still avoid aliasing.

\subsection{Aliasing frequency}

Temporal aliasing is a major concern in the sampling of video and audio signals. Music, for instance, may contain high-frequency components that are inaudible to humans. If a piece of music is sampled at 32000 samples per second (Hz), any frequency components above 16000 Hz (the Nyquist frequency for this sampling rate) will cause aliasing when the music is reproduced by a digital to analog converter (DAC), giving audible distortion. In principle we can compute the aliasing frequency of the distortion. To that end, reconsider the example of Figure \ref{fig:aliasing}, which is copied here for convenience.
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.6\linewidth]{figures/1920px-AliasingSines.png}
    \caption{Two different sinusoids that fit the same set of samples. (\small{source: \href[]{http://en.wikipedia.org/wiki/Aliasing}{Wikipedia}}).}
\end{figure}
Nine cycles of the red sinusoid and one cycle of the blue sinusoid span an interval of ten samples. The corresponding number of cycles per sample are $f_{red} = 0.9$ and $f_{blue} = 0.1$. Therefore it is unclear, whether these samples were produced by sampling functions $\cos{(2 \pi 0.9 t - \theta)}$ and $\cos{(2 \pi 0.1 t - \theta)}$. Note even, that if these samples were produced by sampling functions  $\cos{(2 \pi 0.9 t - \theta)}$ and $\cos{(2 \pi 0.1 t - \theta)}$, they could also have been produced by the trigonometrically identical functions  $\cos{(2 \pi (-0.9) t - \theta)}$ and $\cos{(2 \pi (-0.1) t - \theta)}$, which again demonstrates the useful concept of negative frequency. 

In general, when a sinusoid of frequency $f$ is sampled with frequency $f_s$, the resulting number of cycles per sample is $f / f_s$  (known as {\bf normalized frequency}), and the samples are indistinguishable from those of another sinusoid (called an alias) whose normalized frequency differs from $f / f_s$ by any integer (positive or negative). Replacing negative frequency sinusoids by their equivalent positive frequency representations, we can express all the aliases of frequency $f$ as the N-th {\bf aliasing frequency} of $f$, i.e.
\begin{equation} \label{eq:aliasing_freqs}
    f_N (f) = \vert f - N f_s \vert , N \in \mathbb{N} 
\end{equation}
for any integer $N$. Another way to interprete aliasing is by realising that when a signal with $f>f_{nyq}$ is sampled, we can project this frequency by moving on the unit circle past the point $z=-1$ and the number of rotation over the unit circle equals $N = \lfloor f / f_s \rfloor$.

\begin{exmp}
    Consider a DSP system that runs at a sampling frequency of $f_s=1\si{\MHz}$, so $f_{nyq} = 500 \si{\kHz}$. What will happen when a sinusoid of $f=2.4\si{\MHz}$ is applied to this system, if no proper precautions are taken?

    Since $f > f_{nyq}$, aliasing will occur. By testing Equation \ref{eq:aliasing_freqs} for different values of $N$, we can find the aliasing frequency the system will produce:
    \begin{align*} 
        f_0 (2.4\si{\MHz}) & = \vert 2.4\si{\MHz} - 0 \cdot 1\si{\MHz} \vert = 2.4\si{\MHz} > f_{nyq}\\
        f_1 (2.4\si{\MHz}) & = \vert 2.4\si{\MHz} - 1 \cdot 1\si{\MHz} \vert = 1.4\si{\MHz} > f_{nyq}\\
        f_2 (2.4\si{\MHz}) & = \vert 2.4\si{\MHz} - 2 \cdot 1\si{\MHz} \vert = 0.4\si{\MHz} < f_{nyq}\\
        f_3 (2.4\si{\MHz}) & = \vert 2.4\si{\MHz} - 3 \cdot 1\si{\MHz} \vert = 0.6\si{\MHz} > f_{nyq}\\
        f_4 (2.4\si{\MHz}) & = \vert 2.4\si{\MHz} - 4 \cdot 1\si{\MHz} \vert = 1.6\si{\MHz} > f_{nyq}\\
        \ldots
    \end{align*}
    So, in this example, the aliasing frequency equals $f_2 (2.4\si{\MHz}) = 0.4\si{\MHz}$.
\end{exmp}


\subsection{Folding}

If we consider a signal spectrum that reaches beyond the Nyquist frequency, computing aliasing frequencies does not always provide sufficient insight. For that reason, we turn to a graphical representation of the signal magnitude and interprete the effect of aliasing. Recall Figure \ref{fig:spectrum_interval}; during sampling the base band spectrum of the sampled signal is mirrored to every multifold of the sampling frequency. If the signal spectrum reaches farther than half the sampling frequency base band spectrum and aliases touch eachother and the base band spectrum gets superimposed by the first alias spectrum, as sketched in Figure \ref{fig:folding}.
\begin{figure}[ht]
    \centering
    \includegraphics[scale=1]{figures/spectrum_folding.eps}
    \caption{Sketch of spectral folding.} \label{fig:folding}
\end{figure}
Note that no matter what function we choose to change the amplitude vs frequency, the graph will exhibit symmetry between 0 and $f_s$. This symmetry is commonly referred to as folding, and another name for $f_s/2$ (the Nyquist frequency) is folding frequency. The grey area under the curve is the part of the spectrum where distortion due to aliasing occurs.


\section{The discrete Fourier transform (DFT)}

The Fourier transform of a function produces a frequency spectrum which contains all of the information about the original signal, but in a different form. The discrete-time Fourier transform (DTFT) is a form of Fourier analysis that is applicable to the uniformly-spaced samples of a continuous function. The term discrete-time refers to the fact that the transform operates on discrete data (samples) whose interval often has units of time. From only the samples, it produces a function of frequency that is a periodic summation of the continuous Fourier transform of the original continuous function. Under certain theoretical conditions, described by the sampling theorem, the original continuous function can be recovered perfectly from the DTFT and thus from the original discrete samples. Recall from Equation \ref{eq:DTFT_transform} that the DTFT of $x[n]$ can be expressed as:
\begin{equation*} 
    X(e^{j \omega T}) = \sum_{n=-\infty}^\infty x[n] e^{-j \omega n T} .
\end{equation*}
The DTFT itself is a continuous function of frequency, but discrete samples of it can be readily calculated via the discrete Fourier transform (DFT), which is by far the most common method of modern Fourier analysis. The {\bf discrete Fourier transform} converts a finite sequence of equally-spaced samples of a function into a same-length sequence of equally-spaced samples of the DTFT, which is a complex-valued function of frequency. The DFT of a sequence of samples, $x[n]$, for $0 \le n < N$ can be expressed as:
\begin{align}  \label{eq:DFT}
    X_k 
    & = \mathcal{F} (x[n]) \\
    & = \sum_{n=0}^{N-1} x[n] e^{-2 \pi j \frac{k n} {N}} \nonumber \\
    & = \sum_{n=0}^{N-1} x[n] \left (\cos{(2 \pi {k n}/{N})} - j \sin{(2 \pi {k n}/{N})} \right ) , \nonumber
\end{align}
where the last expression follows from the first one by Euler's formula. In Figure \ref{fig:DFT}, an example of the amplitude of DFT coefficients is plotted. Note that the fundamental interval now is defined from $k=0...N-1$ as dictated by Equation \ref{eq:DFT}. In practice, one would typically consider only the first half of the DFT coefficents, resembling the single-sided spectrum.

\begin{figure}[ht]
    \centering 
    \includegraphics[scale=1]{figures/spectrum_DFT.eps}
    \caption{Sketch of a DFT of a discrete-time signal.} \label{fig:DFT}
\end{figure}

The DFT is the most important discrete transform, used to perform Fourier analysis in many practical applications. In digital signal processing, the function is any quantity or signal that varies over time, such as the pressure of a sound wave, a radio signal, or daily temperature readings, sampled over a finite time interval (often defined by a window function). In image processing, the samples can be the values of pixels along a row or column of a raster image. The DFT is also used to efficiently solve partial differential equations, and to perform other operations such as convolutions or multiplying large integers.

Since the DFT deals with a finite amount of data, it can be implemented in computers by numerical algorithms or even dedicated hardware. These implementations usually employ efficient fast Fourier transform (FFT) algorithms; so much so that the terms "FFT" and "DFT" are often used interchangeably. The FFT has seen wide usage across a large number of fields; we only sketch a few examples below (see also the references \href[]{http://en.wikipedia.org/wiki/Discrete_Fourier_transform#Applications}{here}). 
\begin{itemize}
    \item {Spectral analysis using FFT, also referred to as frequency domain analysis or spectral density estimation, is the technical process of decomposing a complex signal into simpler parts, since many physical processes are best described as a sum of many individual frequency components. Spectrum analysis can be performed on the entire signal. Alternatively, a signal can be broken into short segments (sometimes called frames), and spectrum analysis may be applied to these individual segments}
    \item Data compression: Several lossy image and sound compression methods employ the DFT: the signal is cut into short segments, each is transformed, and then the Fourier coefficients of high frequencies, which are assumed to be unnoticeable, are discarded. The decompressor computes the inverse transform based on this reduced number of Fourier coefficients. Compression applications often use a specialized form of the DFT, such as the discrete cosine transform, or Wavelet transform.
    \item Efficient convolution, i.e. when data is convolved with a very long but finite impuse response, it may be faster to transform it, multiply pointwise by the transform of the filter and then reverse transform it.
\end{itemize}



% en.wikipedia.org/wiki/Spectral_density_estimation

\begin{exmp}
    Let's use the FFT to find the frequency components of a signal buried in noise. In Listing \ref{code:noisy_FFT}, a script is given for simulation of a signal in noise, as plotted in Figure \ref{fig:exp_sig_noise}. Obviously, it is difficult to identify the frequency components by looking at the signal in the time-domain. Listing \ref{code:noisy_FFT} also shows how to compute the signal's single-sided amplitude spectrum and Figure \ref{fig:FFT_exp_sig_noise} the result of this exercise is plotted.
    \begin{figure}[hb]
        \centering 
        \includegraphics[width=0.6\linewidth]{figures/signal_corrupted_by_noise.eps}
        \caption{Example of a signal corrupted by noise.} \label{fig:exp_sig_noise}
    \end{figure}
    \begin{figure}[ht]
        \centering 
        \includegraphics[width=0.6\linewidth]{figures/single_sided_spectrum_signal_corrupted_by_noise.eps}
        \caption{Single-sided amplitude spectrum of the signal depicted in Figure \ref{fig:exp_sig_noise}.} \label{fig:FFT_exp_sig_noise}
    \end{figure}    
    \lstinputlisting[float, language=matlab, style=Matlab-editor, caption={Matlab script for FFT of a noisy signal {source: \href[]{http://nl.mathworks.com/help/matlab/ref/fft.html}{MATLAB}}).}, label=code:noisy_FFT]{scripts/fft_noisy_signal_example.m}   
\end{exmp}


% \subsection{Spectral leakage and windowing}
% The Fourier transform of a function of time, s(t), is a complex-valued function of frequency, S(f), often referred to as a frequency spectrum. Any linear time-invariant operation on s(t) produces a new spectrum of the form H(f)•S(f), which changes the relative magnitudes and/or angles (phase) of the non-zero values of S(f). Any other type of operation creates new frequency components that may be referred to as spectral leakage in the broadest sense. Sampling, for instance, produces leakage, which we call aliases of the original spectral component. For Fourier transform purposes, sampling is modeled as a product between s(t) and a Dirac comb function. The spectrum of a product is the convolution between S(f) and another function, which inevitably creates the new frequency components. But the term 'leakage' usually refers to the effect of windowing, which is the product of s(t) with a different kind of function, the window function. Window functions happen to have finite duration, but that is not necessary to create leakage. Multiplication by a time-variant function is sufficient.

% Leakage caused by a window function is most easily characterized by its effect on a sinusoidal s(t) function, whose unwindowed Fourier transform is zero for all but one frequency. The customary frequency of choice is 0 Hz, because the windowed Fourier transform is simply the Fourier transform of the window function itself...
% % en.wikipedia.org/wiki/Spectral_leakage
% % en.wikipedia.org/wiki/Window_function

\section{Return to the time-domain}
The discrete Fourier transform is an invertible, linear transformation, i.e. the inverse DFT is the original sampled data sequence. The inverse DFT (IDFT) is given by
\begin{align}  \label{eq:iDFT}
    x[n]
    & = \mathcal{F}^{-1} (X[k]) \\
    & = \frac{1}{N} \sum_{k=0}^{N-1} X_k e^{+2 \pi j \frac{k n} {N}} \nonumber . 
\end{align}
A useful property of the DFT is that the inverse DFT can be easily expressed in terms of the (forward) DFT, via several well-known "tricks", which allow the inverse FFT to be directly obtained from the FFT.

\section{Further reading}
For more information, see e.g.
\begin{itemize}
    \item \url{http://en.wikipedia.org/wiki/Finite_impulse_response}
    \item \url{http://en.wikipedia.org/wiki/Z-transform#Relationship_to_Fourier_series_and_Fourier_transform}
    \item \url{http://en.wikipedia.org/wiki/Negative_frequency}
    \item \url{http://en.wikipedia.org/wiki/Aliasing}
    \item \url{http://en.wikipedia.org/wiki/Geometric_series#Sum}
    \item \url{http://en.wikipedia.org/wiki/L%27H%C3%B4pital%27s_rule}
    \item \url{http://en.wikipedia.org/wiki/Discrete-time_Fourier_transform}
    \item \url{http://en.wikipedia.org/wiki/Discrete_Fourier_transform}
    \item \url{http://en.wikipedia.org/wiki/Spectral_density_estimation}
    \item \url{http://en.wikipedia.org/wiki/Spectral_leakage}
    \item \url{http://en.wikipedia.org/wiki/Window_function}
\end{itemize}

