\chapter{The z-domain} \label{chap:z_domain}

\section{Introduction}

Analyzing digital signal processing system in the time-domain can be extremely difficult. Especially, recursive filters lead to complicated differential equations that be can be hard to solve. For that reason DSP engineers turn to other domains, such as by applying the z-transform to arrive at the z-domain. The z-transform is analogous to the Laplace transform, which is used to design and analyze analog filters. A graphical representation called the z-plane pole-zero plot is frequently used, which can be considered as the discrete equivalent of the familiar s-plane. 

The z-domain makes analysis and design of DT system much easier, and also the process of convoluting, i.e. computing the response of a system to an input signal. For these reasons, the z-transform is widely used in DSP literature. Digital filters come in both IIR and FIR types. FIR filters have many advantages, but are computationally more demanding. Whereas FIR filters are always stable, IIR filters have feedback loops that may become unstable or oscillate. The z-transform provides a tool for analyzing stability issues of digital IIR filters. 

% TODO? interchange number of zeroes (P) and number of poles (Q)
% toDO? change number of zeros to N
%

\section{The z-transform}

In mathematics and signal processing, the z-transform converts a discrete-time signal, which is a sequence of real or complex numbers, into a complex frequency-domain representation. It can be considered as a discrete-time equivalent of the Laplace transform. The z-transform gives a tractable way to solve linear, constant-coefficient difference equations.

Under the assumption that $x[n]$ is defined only for $n \ge 0$, the unilateral z-transform of a discrete-time signal $x[n]$ is the formal power series $X(z)$ defined as:
\begin{align} \label{eq:z_transform}
    X(z)
    & = \sum_{n=0}^\infty x[n] z^{-n} \\
    & = z[n] + z^{-1} x[1] + z^{-2} x[2] + z^{-3} x[3] + \ldots \nonumber \\
    & = \mathcal{Z} \{ x[n] \}  \nonumber  ,
\end{align}
where $z$ is, in general, a complex number:
\begin{align} \label{eq:def_z}
    z 
    & = A e^{j \phi} \\
    & = A \left ( \cos(\phi) + j \sin(\phi) \right ) \nonumber ,
\end{align}
where $A$ is the magnitude of $z$, and $\phi$ is the complex argument (also referred to as angle or phase) in radians. Note that $X(z)$ is also a complex number and a continuous function of $z$. Further notice that the parentheses indicate that $X(z)$ is a continuous function of $z$, not discrete. Even though we are dealing with a discrete time domain signal, $A$ and $\phi$
can still take on a continuous range of values.   

The z-transform cannot be determined for every signal or system, i.e. a Region of Convergence (ROC) must exist within the z-domain. In this workshop we will not dive into this, but bare in mind that we will only deal with causal ROCs. More information can be found \href{http://en.wikipedia.org/wiki/z-transform#Example_2_(causal_ROC)}{here}.

\begin{exmp}
    Consider a sequence of $4$ samples
    \begin{equation*}
        x[0] = .5, x[1] = 3.0, x[2] = -0.5, x[3] = 1.0 .
    \end{equation*}
    The discrete-time signal can be expressed as:
    \begin{equation*}
        x[n] = .5 \delta[n] + 3.0 \delta[n-1] - 0.5 \delta[n-2] + \delta [n-3]
    \end{equation*}
    The z-transform of $x[n]$ equals:
    \begin{align*}
        X(z) 
        & = \mathcal{Z} \{ x[n] \}\\
        & = \sum_{n=0}^\infty x[n] z^{-n} \\
        & = .5 + 3.0 z^{-1} - 0.5 z^{-2} + z^{-3} .
    \end{align*}
    So, in the z-domain discrete time-delays are replaced by multiplication by $z^{-n}$, which is one of the benefits of the z-transform.
\end{exmp}

The z-transform has many useful properties, that can for instance be found \href{http://en.wikipedia.org/wiki/Z-transform#Properties}{here}. In this workshop we will only need a few, summarized in table \ref{tab:z_transform_properties}.
\def\arraystretch{2.5}%  1 is the default, change whatever you need
\begin{table}[h!]
    \centering
    \begin{tabular}{|l|l|l|}
        \hline
        & Time-domain & z-domain \\
        \hline
        Notation & $x[n] = \mathcal{Z}^{-1} \{ X(z) \}$ & $X(z) = \mathcal{Z} \{ x[n] \}$ \\
        \hline
        Linearity & $a_1 x_1[n] + a_2 x_2[n]$ &  $a_1 X_1(z) + a_2 X_2(z)$ \\ \hline 
        Delay & $x[n-k]$, with $k>0$ &  $z^{-k} X(z)$ \\ \hline
        Convolution & $x_1 [n] * x_2[n]$ & $X_1(z) X_2(z)$ \\ \hline 
    \end{tabular}
    \caption{Properties of the z-transform.} \label{tab:z_transform_properties} 
\end{table}

In this workshop we will not explicitly consider the inverse z-transform, $x[n] = \mathcal{Z}^{-1} \{ X(z) \}$. Instead, we will piggyback on common z-transform pairs and use those to provide further understanding of discrete-time systems. To that end, another basic signal is introduced first, namely the Heaviside step function or unit step function,
\begin{equation} \label{eq:unit_step}
        u[n] = 
            \left \{ 
                \begin{array}{ll}
                    0, & \text{if } n < 0\\
                    1, & \text{if } n \ge 0
                \end{array}
            \right . ,
\end{equation}
which represents a signal that switches on at a specified sample number, $n=0$, and stays switched on indefinitely. This signal is often introduced to enforce causality and thus allow real-time signal processing. A causal filter uses only previous samples of the input or output signals. So, multiplying any impulse response by a unit step function, gives a causal result.

In Table \ref{tab:z_transform_pairs}, basic z-transform pairs that are used throughout this workshop are listed. Proofs of these transform pairs as well as comprehensive lists can be found elsewhere, e.g. 
\href{http://en.wikipedia.org/wiki/Z-transform#Table_of_common_Z-transform_pairs}{here}.
\begin{table}[h!]
    \centering
    \begin{tabular}{|l|l|l|}
        \hline
        & Time-domain & z-domain \\
        \hline
        Notation & $x[n] = \mathcal{Z}^{-1} \{ X(z) \}$ & $X(z) = \mathcal{Z} \{ x[n] \}$ \\
        \hline
        Kronecker delta function & $\delta[n]$ & 1 \\
        \hline
        Unit step function & $u[n]$ & $\frac{z}{z-1}$\\
        \hline
        Geometric sequence & $a^n u[n]$ & $\frac{z}{z-a}$\\
        \hline
        Delayed function & $x[n-k]$ &  $z^{-k} X(z)$ \\
        \hline
    \end{tabular}
    \caption{z-transform pairs.} \label{tab:z_transform_pairs} 
\end{table}

\begin{exmp}
    A time-domain signal is defined by the following sequence of samples:
    \begin{equation*}
        x[n] = - \delta [n] + 2 \delta[n-1] - 3 \delta[n-5] .
    \end{equation*}
    Now, using the linearity and delay properties as well as the z-transform of the Kronecker delta, the z-transform $X(z)$ can be expressed as: 
    \begin{align*}
        X(z) & = -1 +2z^{-1} -3 z^{-5} .
    \end{align*}
\end{exmp}

\begin{exmp}
    By way of illustration, the derivation of the z-transform of a geometric sequence is given here
    \begin{align*}
        \mathcal{Z} \{ a^n u[n] \}  
        & = \sum_{n=0}^\infty a^n u[n] z^{-n} = \sum_{n=0}^\infty a^n z^{-n} \\
        & = \sum_{n=0}^\infty \left ( a z^{-1} \right )^n \\
        & = \frac{1} {1-a z^{-1}} = \frac{z} {z-a} ,
    \end{align*}
    where the finite sum formula of an infinite geometric series is used, i.e.
    \begin{equation*}
        \sum_{k=0}^\infty r^k = \frac {1} {1-r} .
    \end{equation*} 
    An infinite geometric series is an infinite series whose successive terms have a common ratio. Such a series converges if and only if the absolute value of the common ratio is less than one ($|r| < 1$).  Note that this convergence criterium also holds for the z-transform, restricting the magnitude of the common value below unity. Loosely speaking, this implies that $-1 < a < 1$ in the geometric sequence. This is an important notion, to which we will return to later on.
\end{exmp}



\begin{exmp}
    Consider the time-domain signal:
    \begin{equation*}
        x[n] = 
            \left \{ 
                \begin{array}{ll}
                    2, & \text{if } n > 0 \text{ and even}\\
                    0, & \text{otherwise}
                \end{array}                
            \right . .
    \end{equation*}
    Now, the definition of the unilateral z-transform is used to derive $X(z) = \mathcal{Z} \{ x[n] \}$ :
    \begin{align*}
        X(z) & =\sum_{n=0}^\infty x[n] z^{-n} \\
        & = 2 z^{-2} + 2 z^{-4} + 2 z^{-6} + \ldots \\
        & = 2 z^{-2} \left ( 1+ z^-{2} + z^{-4} + \ldots \right ) \\
        & = 2 z^{-2} \sum_{n=0}^\infty{z^{-2n}} = 2 z^{-2} \sum_{n=0}^\infty{(z^{-2})^n}\\
        & = \frac {2 z^{-2} } {1-z^{-2}} .
    \end{align*}
\end{exmp}


\section{Transfer functions}

Recall that the general difference equation \ref{eq:diff_eq_cond} cannot be easily solved in the time-domain. Now consider the balanced equation \ref{eq:diff_eq_bal}, which is repeated here for convenience, 
\begin{equation*}
    \sum_{j=0}^Q a_j y[n-j] = \sum_{i=0}^P b_i x[n-i] .
\end{equation*}
The difference equation shows how to compute the next output sample, $y[n]$, in terms of the past outputs, $y[n-j]$, the present input, $x[n]$, and the past inputs, $x[n-i]$. Taking the z-transform of the above equation (using linearity and time-shifting properties) yields 
\begin{equation*}
    Y(z) \sum_{j=0}^Q a_j z^{-j} = X(z) \sum_{i=0}^P b_i z^{-j} .
\end{equation*}
and rearranging results in the transfer function (TF), 
\begin{equation} \label{eq:transfer_function}
    H(z) = \frac{Y(z)}{X(z)} = \frac{\sum_{i=0}^P b_i z^{-j}} {\sum_{j=0}^Q a_j z^{-j}} = \frac{b_0 + b_1 z^{-1} + b_2 z^{-2} + \cdots + b_P z^{-P}}{a_0 + a_1 z^{-1} + a_2 z^{-2} + \cdots + a_Q z^{-Q}}.
\end{equation}
This is the form for a recursive filter, which typically leads to an infinite impulse response, but if the denominator is made equal to unity i.e. no feedback, then this becomes an FIR filter. 

A digital filter is characterized by its transfer function (see Figure \ref{fig:DT_filter_z_domain}) in the complex z-domain, or equivalently in the time-domain, its difference equation or impulse response. Consequently, the transfer function is the z-transform of the impulse response, i.e.
\begin{equation}
    H(z) = \mathcal{Z} \{ h[n] \} .
\end{equation}
Recall that to determine an output directly in the time-domain requires the convolution of the input signal with the filter impulse response, i.e.
\begin{equation*}
    y[n] = h[n] * x[n] .
\end{equation*}
When the transfer function and the z-transform of the input are known, this convolution may be more complicated than the alternative of multiplying two functions in the z-domain, i.e. 
\begin{equation*}
    Y(z) = H(z) X(z) .
\end{equation*}
In this workshop, we will not go into further detail on how and when to compute a filter output beyond the time-domain.
\begin{figure}[t!]
    \centering
    \includegraphics[scale=1]{figures/DT_filter_z_block_diagram.eps}
    \caption{Blockdiagram of a discrete-time filter.} \label{fig:DT_filter_z_domain}
\end{figure}
Analysis of the transfer function can describe how a system will respond to any input. As such, designing a filter consists of developing specifications appropriate to the problem (for example, a second-order low pass filter with a specific cut-off frequency), and then producing a transfer function which meets the specifications. Note that the transfer function is also known as the system function or network function, since the term "transfer function" is also used in the frequency-domain analysis of systems using transform methods such as the Laplace transform; here it means the amplitude of the output as a function of the frequency of the input signal.

\def\arraystretch{2.5}%  1 is the default, change whatever you need
\begin{table}[h!]
    \centering
    \begin{tabular}{|l|l|c|}
        \hline
        Addition & $y[n] = x_1[n] + x_2[n]$ & \includegraphics[scale=0.75]{figures/adder_z.eps} \\ \hline 
        Multiplication & $y[n] = a x[n]$ & \includegraphics[scale=0.75]{figures/multiplyer_z.eps}\\ \hline
        Delay & $y[n] = x[n-1]$ & \includegraphics[scale=0.75]{figures/delay_z.eps}\\ \hline        
    \end{tabular}
    \caption{Basic components of discrete-time filters in the z-domain.} \label{tab:z_filter_components}
\end{table}
The basic properties listed in Table \ref{tab:z_transform_properties} can be used to transform a block diagram from the time-domain to the z-domain. Because of linearity, we can transform the three basic mathematical operators separately. In Table \ref{tab:z_filter_components}, the z-transform of the different operators are listed, together with their block-diagram representation, which you will typically find in DSP literature.

\begin{exmp}
    Again, let's look at the example of the 3 point moving average filter. First the blockdiagram is transformed to the z-domain (see Figure \ref{fig:Blockdiagram_z_MA_filter}). Following the basic properties given in Table \ref{tab:z_transform_properties}, we can describe the input and output signals by their corresponding z-transforms, $X(z) = \mathcal{Z} \{ x[n] \}$ and $Y(z) = \mathcal{Z} \{ y[n] \}$, respectively. Because of the linearity property, the addition operator and multiplication operator (by a constant, that is) do not change under the z-transform. A unit time-delay $T$, however, is replaced by multiplication with $z^{-1}$. Now the response of the system in the z-domain can be expressed as:
    \begin{align*}
        Y(z) 
        & = \frac{1}{3} X(z) + \frac{1}{3} z^{-1} X(z) + \frac{1}{3} z^{-2} X(z) \\
        & = \frac{X(z)}{3} \left (1 + z^{-1} + z^{-2} \right ) ,
    \end{align*}
    Thus, the transfer function equals:
    \begin{align} \label{eq:TF_MA}
        H(z) 
        & = \frac{Y(z)}{X(z)} \nonumber \\
        & = \frac{1}{3} \left (1 + z^{-1} + z^{-2} \right ) \nonumber \\
        & = \frac{z^2 + z + 1}{3z^2} ,
    \end{align}
    the latter being a more common representation, avoiding negative exponents. Note that for FIR filters, in principle there is no denominator in the transfer function and the filter order is merely the number of delays used in the filter structure. 
    \begin{figure}[t!]
        \centering
        \includegraphics[scale=1]{figures/MA_filter_z_block_diagram.eps}
        \caption{Blockdiagram of an MA filter.} \label{fig:Blockdiagram_z_MA_filter}
    \end{figure}
\end{exmp}

\begin{exmp}
    The transfer function of an IIR filter contains a non-trivial denominator, describing feedback terms. Let's return to the example depicted in Figure \ref{fig:Blockdiagram_DC_remover}. Again, the blockdiagram is transformed to the z-domain first, see Figure \ref{fig:Blockdiagram_z_DC_remover}. Now the response of the system in the z-domain can be expressed as:
    \begin{equation*}
        Y(z) = X(z) - z^{-1} X(z) + z^{-1} a Y(z) ,
    \end{equation*}
    separating variables yields:
    \begin{equation*}
        Y(z) (1 - a z^{-1}) = X(z) (1- z^{-1}) .
    \end{equation*}
    Thus, the transfer function equals:
    \begin{equation} \label{eq:TF_DC_blocker}
        H(z) 
        = \frac{Y(z)}{X(z)}
        = \frac{1- z^{-1}}{1 - a z^{-1}}
        = \frac{z-1}{z-a} .
    \end{equation}
    \begin{figure}[b!]
        \centering
        \includegraphics[scale=1]{figures/DC_remover_z_block_diagram.eps}
        \caption{Blockdiagram of an IIR high-pass filter.} \label{fig:Blockdiagram_z_DC_remover}
    \end{figure}
\end{exmp}

\begin{exmp}
    Consider a second order IIR filter, depicted in Figure \ref{fig:Blockdiagram_z_Goertzel}. In order to derive the transfer function, first the output of the first adder is defined as a helper signal $R(z)$, 
    \begin{equation*}
        R(z) = X(z) + a z^{-1} R(z) + c z^{-2} R(z) ,
    \end{equation*}
    separating variables yields:
    \begin{equation*}
        R(z) (1 - a z^{-1} - c z^{-2}) = X(z) .
    \end{equation*}
    So the transfer from $X(z)$ to $R(z)$ equals,
    \begin{equation*}
        \frac{R(z)} {X(z)} = \frac{1} {1 - a z^{-1} - c z^{-2}}
    \end{equation*}    
    Let's now express the output of the system in terms of $R(z)$, i.e.
    \begin{equation*}
        Y(z) = R(z) + b z^{-1} R(z) ,
    \end{equation*}
    and,
    \begin{equation*}
        \frac{Y(z)} {R(z)} = 1 + b z^{-1},        
    \end{equation*}
    so the transfer function equals
    \begin{equation*}
        H(z) = \frac {Y(z)} {X(z)} = \frac {Y(z)} {R(z)} \frac {R(z)} {X(z)} =  \frac{1 + b z^{-1}} {1 - a z^{-1} - c z^{-2}}
    \end{equation*}
    \begin{figure}[ht]
        \centering
        \includegraphics[scale=1]{figures/Goertzel_z_block_diagram.eps}
        \caption{Blockdiagram of an IIR high-pass filter.} \label{fig:Blockdiagram_z_Goertzel}
    \end{figure}  
\end{exmp}


\section{The z-plane}

A transfer function (Equation \ref{eq:transfer_function}) can be written as the ratio of a numerator $N(z)$ and a denominator $D(z)$ polynomial in $z$. The numerator has $Q$ roots (corresponding to zeros of H) and the denominator has $P$ roots (corresponding to poles). Apart from a gain factor $K$, the transfer function may be rewritten in terms of zeros and poles:
\begin{equation} \label{eq:transfer_function_poles_zeroes}
    H(z) = \frac{N(z)}{D(z)} = K \frac{(z-z_1)(z-z_2) \cdots (z-z_Q)} {(z-p_1)(z-p_2) \cdots (z-z_P)} ,
\end{equation}
where $z_i$ is the i-th zero and $p_j$ is the j-th pole of the system. As the term suggests, a zero is a value of $z$ where $H(z) = 0$, so where the numerator $N(z)=0$. On the other hand, a pole is a certain type of singularity of a function, where the denominator $D(z)=0$.

The zeros and poles are commonly complex, in addition, there may also exist zeros and poles at $z = 0$ and $z = \infty$. If we take these poles and zeros as well as multiple-order zeros and poles into consideration, theoretically, the number of zeros and poles are always equal. However, in DSP engineering it is more common to just equal the number of zeros to the feed-forward order, and the number of poles to the feedback order.

A {\bf pole–zero plot} is a graphical representation of a rational transfer function in the complex plane, which helps to convey certain properties of the system, such as stability and causality. A pole-zero plot shows the location in the complex plane of the poles and zeros of the transfer function. For a DT system, the plane is the z-plane, where $z$ represents the domain of the z-transform. 
By convention, the poles of the system are indicated in the plot by an X while the zeros are indicated by a circle or O. When the system's impulse response is not complex, but completely real (the most common case), the upper and lower halves of the z-plane are mirror images of each other.

\begin{exmp}
    \begin{figure}[t]
        \centering
        \includegraphics[width=0.6\linewidth]{figures/MA_ZP_plot.eps}
        \caption{Pole-zero plot in the complex z-plane of a 3-point MA filter.} \label{fig:z_plot_MA_filter}
    \end{figure}       
    Now, consider the example of a moving average filter, depicted in Figure \ref{fig:Blockdiagram_z_MA_filter}. The transfer function is given by Equation \ref{eq:TF_MA}, which can be defactored as:
    \begin{align*}
        H(z) 
        & = \frac{z^2 + z + 1} {3z^2}\\
        & = \frac{1} {3} \frac{ (z-.5-.866j)(z(.5+ .866j)} {z^2} ,
    \end{align*}
    where the quadratic formula 
    \begin{equation*}
        x = \frac{-b \pm  \sqrt{b^2-4ac}} {2a}
    \end{equation*}
    is applied to find the general solution to a quadratic equation of the form
    \begin{equation*}
        a x^2 + b x + c = 0 .
    \end{equation*}
    Concluding, the zeros, poles, and gain of the discrete-time transfer function are given by:
    \begin{align*}
        z_{1,2} & = .5 \pm.866j \\
        p_{1,2} &  = 0 \text{ (or no poles)}\\
        K & = \frac{1}{3} .
    \end{align*}
    Using MATLAB, we can easily compute the poles, zeroes, and gain by defining the numerator and denominator of the transfer function as seperate arrays, and calling the function \lstinline[language=matlab, style=Matlab-editor]!tf2zpk!. Then, using the function \lstinline[language=matlab, style=Matlab-editor]!zplane! we can directly produce a pole-zero plot in the complex z-plane. In Figure \ref{fig:z_plot_MA_filter}, the pole-zero plot of the MA filter example is plotted. Note that also the unit circle $z=e^{\phi j}$ is depicted, which is the circle of radius one, centered at the origin. As stated before, in engineering it is more common to define a FIR filter to be without poles, since there is no feedback. Thus we would say that the MA filter has no poles. The Matlab script for producing the plot is printed in Listing \ref{code:MA_zplane}.
 
    \lstinputlisting[float, language=matlab, style=Matlab-editor, caption={Matlab script for producing a pole-zero plot of first order IIR filter.}, label=code:MA_zplane]{scripts/MA_zplane.m}       
\end{exmp}

\begin{exmp} 
    The transfer function of an IIR filter contains a non-trivial denominator, which can be described by a number of poles. Let's return to the example of a first-order IIR filter depicted in Figure \ref{fig:Blockdiagram_z_DC_remover} with transfer equation (\ref{eq:TF_DC_blocker})
    \begin{equation*} 
        H(z) 
        = \frac{z-1}{z-a} .
    \end{equation*}
    The zero, pole, and gain of $H(z)$ are given by:
    \begin{align*}
        z_{1} & = 1 \\
        p_{1} & = a\\
        K & = 1 .
    \end{align*}
    The pole-zero plot in the complex z-plane of this simple IIR filter example with $a=.5$ is plotted in Figure \ref{fig:z_plot_DC_remover}.   The Matlab script for producing the plot is printed in Listing \ref{code:IIR1_zplane}. Note that also the function \lstinline[language=matlab, style=Matlab-editor]!impz! is used to produce a plot of the system impulse response, which is depicted in Figure \ref{fig:IR_DC_remover}.
    \begin{figure}[ht]
        \centering
        \includegraphics[width=0.6\linewidth]{figures/DC_remover_z_plane.eps}
        \caption{Pole-zero plot in the complex z-plane of an IIR high-pass filter.} \label{fig:z_plot_DC_remover}
    \end{figure}
     \lstinputlisting[float, language=matlab, style=Matlab-editor, caption={Matlab script for producing a pole-zero plot of second order filter.}, label=code:IIR1_zplane]{scripts/DC_blocker.m}     
\end{exmp}

\begin{exmp}
    Finally, consider a more complicated transfer function of a second-order IIR filter, given by
    \begin{equation*}
        H(z) = \frac{z^2-1}{2z^2+2z+1} .
    \end{equation*}
    Again, the zeroes, poles, and gain can be found by defactoring the numerator and denominator
    \begin{equation} \label{eq:TF_IIR2}
        H(z) = \frac{(z+1)(z-1)}{ (z-.5-.5j) (z- .5-.5j)} = ,
    \end{equation}
    so
    \begin{align*}
        z_{1,2} & = \pm 1 \\
        p_{1,2} & = -.5 \pm .5 j\\
        K & = 1 ,
    \end{align*}
    and the pole-zero plot of this IIR filter example is plotted in Figure \ref{fig:IIR2_ZP_plot}. It should be noted that pole pairs of real filters either are real, or come in complex conjugated pairs, for reasons that will become clear in the next chapter. The Matlab script for producing the plot is printed in Listing \ref{code:IIR2_zplane}. 
    \begin{figure}[ht]
        \centering
        \includegraphics[width=0.6\linewidth]{figures/IIR2_ZP_plot.eps}
        \caption{Pole-zero plot in the complex z-plane of Equation \ref{eq:TF_IIR2}.} \label{fig:IIR2_ZP_plot}
    \end{figure}
    \lstinputlisting[float, language=matlab, style=Matlab-editor, caption={Matlab script for producing a pole-zero plot of an MA filter.}, label=code:IIR2_zplane]{scripts/IIR_example2.m}      
\end{exmp}


\section{Return to the time-domain}

The inverse z-transform of a transfer function equals the impulse response of the system, i.e.
\begin{equation*}
   h[n] = \mathcal{Z}^{-1} \{ H(z) \} .
\end{equation*}
Although the inverse z-transform, is well-described in literature, in this workshop we will not explicitly consider the mathematics involved. Instead, we will apply the z-transform pairs summarized in Table \ref{tab:z_transform_pairs}. 

\begin{exmp}
    Given the transfer function of the second-order FIR MA filter (Equation \ref{eq:TF_MA}), we can now determine the impulse response using Table \ref{tab:z_transform_pairs}:
    \begin{align*}
        h[n] = \mathcal{Z}^{-1} \{ H(z) \} 
        & = \mathcal{Z}^{-1} \left \{ \frac{1}{3} \left (1 + z^{-1} + z^{-2} \right ) \right \} \\ 
        & = \frac{1}{3} \delta[n] + \frac{1}{3} \delta[n-1] + \frac{1}{3} \delta[n-2] .
    \end{align*}
    Seeing that a Kronecker delta pulse represents a single sample, we have found the mathematical description of the MA filter's impulse response as depicted in Figure \ref{fig:IR_MA_filter}. It is evident that this procedure can be repeated for an arbirtray but finite number of feedforward coefficients to arrive at Equation \ref{eq:IR_FIR}.
\end{exmp}
\begin{exmp}
    Consider the first-order IIR filter depicted in Figure \ref{fig:Blockdiagram_z_DC_remover} with transfer equation (\ref{eq:TF_DC_blocker}), which is repeated here:
    \begin{equation*} 
        H(z) 
        = \frac{z-1}{z-a} .
    \end{equation*}
    We can now determine the impulse response of the system by first rearranging terms, such that they correspond to linear combination of z-transform pairs given in the Table \ref{tab:z_transform_pairs} 
    \begin{align*}
        h[n] & = \mathcal{Z}^{-1} \left \{ H(z) \right \} \\
        & = \mathcal{Z}^{-1} \left \{ \frac{z-1}{z-a} \right \} \\ 
        & = \mathcal{Z}^{-1} \left \{ \frac{z}{z-a} \right \} - \mathcal{Z}^{-1} \left \{ z^{-1} \frac{z}{z-a} \right \} .    
    \end{align*}
    Then, using the z-transform table we arrive at the mathematical description of the impulse response as depicted in Figure \ref{fig:IR_DC_remover}, namely:
    \begin{equation*}
        h[n] = a^n u[n] - a^{n-1} u[n-1] .
    \end{equation*}
    A stated before, the MATLAB function \lstinline[language=matlab, style=Matlab-editor]!impz! can also be used to determine the system impulse response, which is depicted in Figure \ref{fig:IR_DC_remover}.
\end{exmp}

\noindent In conclusion: it is much easier to solve the linear difference equation of a system using a detour via the z-domain, to arrive at an expression of an impulse response of a recursive filter. For IIR filters of higher order, partial fraction decomposition can be applied to factor the denominator, which can then be transformed back to the time domain. In this workshop we will not go into further detail on inverse transformation of such higher order IIR structures.


\section{Stability}

The systems discusse in this workshop need to be bounded-input, bounded-output (BIBO) stable to be of any practical use. The transfer function and the pole-zero plot allow one to judge whether or not a system is stable. If a system is BIBO stable, then the output will be bounded for every input to the system that is also bounded. Recall from Equation \ref{eq:DT_stability} that for a DT system, the condition for stability can be denoted as 
\begin{equation*}
    \sum_{k=-\infty}^{\infty} \left | h[k] \right | < \infty .
\end{equation*}
For a causal system, to guarentee stability all poles of the transfer function have to have an absolute value smaller than one. In other words, all {\bf poles must be located within the unit circle}, $z=e^{\phi j}$, in the z-plane, which is why the unit circle is always depicted in a pole-zero-plot. The circle is the edge between the stable and the unstable regions, see Figure \ref{fig:stability_zplane}.
\begin{figure}[ht]
    \centering
    \includegraphics[scale=1]{figures/stability.eps}
    \caption{Stable and unstable regions for pole locations in the z-plane.} \label{fig:stability_zplane}
\end{figure}


\begin{exmp}
    Consider the familiar geometric sequence $a^n u[n]$, previously listed in Table \ref{tab:z_transform_pairs}, which can be regarded as the impulse response of a first-order IIR filter. The transfer function of this system can be readily copied from the table and equals:
    \begin{equation*}
        H(z) = \mathcal{Z} \{ a^n u[n] \} = \frac{z} {z-a} .
    \end{equation*}
    So what happens if $a > 1$? Now, the impulse response will not converge to a stable value, but diverge with time. The filter can produce an output that grows without bounds, with bounded or even zero input, and therefore the system is unstable.

    When the pole is located on the unit circle, i.e. $a=1$ for the previous example, then the filter is said to be marginally stable. With poles located on the unit circle, a system can produce an output that converges to a constant value with time, or remains bounded within a finite interval. However, the impulse response is not absolutely summable. This special case will not be considered further in this workshop.
\end{exmp}


\section{Further reading}
% For more information on the z-transform, see e.g. 
\begin{itemize}
    \item \url{http://en.wikipedia.org/wiki/Z-transform}
    \item \url{http://en.wikipedia.org/wiki/Pole%E2%80%93zero_plot}
    \item \url{http://en.wikipedia.org/wiki/Quadratic_equation}
    \item \url{http://en.wikipedia.org/wiki/Partial_fraction_decomposition}
    \item \url{http://en.wikibooks.org/wiki/Digital_Signal_Processing}
\end{itemize}

