\chapter{Workshop preface}

\section{Introduction}

Digital signal processing (DSP) is the use of digital processing, such as by computers or more specialized digital signal processors, to perform a wide variety of signal processing operations. The signals processed in this manner are a sequence of numbers that represent samples of a continuous variable in a domain such as time, space, or frequency. Suppose we attach an analog-to-digital converter to a computer and use it to acquire a chunk of real-world data. DSP answers the question: What next?

The ultimate goal of this workshop is to provide you with the tools to design, implement, and analyze a simple DSP system, i.e. DSP engineering. To that end, you will first learn about the basics of DSP by focussing on signal sampling in Chapter \ref{chap:basics} and analyzing digital systems in Chapter \ref{chap:discrete-time_domain}. In many cases, proper analysis and design of DSP systems in the time-domain is very hard, if not impossible. For that reason DSP engineers turn to other domains, such as by applying the z-transform to arrive at the z-domain (Chapter \ref{chap:z_domain}), or via the Fourier-transform to arrive in the frequency domain (Chapter \ref{chap:frequency_domain}). In the workshop, you will get accustomed to changing your viewpoint from the time-domain, z-domain, and frequency domain, depending on which perspective is relevant to acquire certain information on a system. Chapter \ref{chap:filter_design} is devoted to digital filter design, here you will learn to use the tools acquired previously to design a digital filter according to given requirements, such a specific frequency response, and such that the filter can be implemented in practice. 

Materials in this reader are adapted from the lemmas on Digital signal Processing on \href[]{en.wikipedia.org/wiki/Digital_signal_processing}{Wikipedia}, the free encyclopedia, and from \href[]{en.wikibooks.org/wiki/Digital_Signal_Processing}{Wikibooks}, the open-content textbooks collection. Text is available under the \href[]{creativecommons.org/licenses/by-sa/3.0/}{Creative Commons Attribution-ShareAlike License}.

\newpage

\section{Organization}

\begin{itemize} 
    \item The DSP workshop is scheduled for 2hr/wk during semester 6 (S6).
    \item The workshop consists of theory with an integrated mini project.
    \item Throughout the semester there will be quizzes that help you to master DSP engineering.
    \item There is no final exam, your grade will be determined by your mini project portfolio  (80\%) and quiz results (20\%).
\end{itemize}




\subsection{DSP miniproject}

In this workshop, you will realize a mini project on DSP engineering with a small team.

\begin{itemize} 
    \item A project team will consist of 2-3 students.
    \item Find a topic, preferably within the context of your S6 project, where the subjects for building a DSP system are covered, i.e. digital signal acquisition, digital filtering, filter design, hard- and software implementation.
    \item Hardware should include at least interfacing electronics and a processing system (MCU, DSP, PC, PLC). 
    \item Software can be implemented using e.g. C, Arduino, Python, etc.
    \item During the semester you will build up your portfolio, by delivering intermediate results as indicated in the schedule.
    \item Finally, in week 2.7 each team will present the system to the group in a 10 min. show\&tell, which includes a live demo (or a movie) of a working set-up.
    \item In week 2.8 each team will deliver your complete portfolio of the project, including, system specifications, design approach, circuitry, software, and test results.
\end{itemize}


\subsection{Quiz}
As indicated in the schedule below, some workshop sessions start with an individual on-line quiz.

\begin{itemize} 
    \item Using your laptop, phone or tablet you will login to this website using your {\bf HAN student number}, so NOT your name, nickname or anything else.
    \item There are 5 questions per multiple choice quiz, concerning the theory that was discussed the week(s) before.
    \item You are allowed to use the course material to answer the questions. 
    \item The quiz is individual and it is not allowed to communicate with your fellow students during the quiz.
    \item Even when you are finished you will remain silent in order not to disturb your fellow students' concentration.
    \item The quiz starts exactly at the beginning of the workshop session and will take 10 minutes. It is expected that you are present before the workshop starts to complete the login procedure.
    \item Once the quiz has started, entrance to class prohibited, in order not to disturb your fellow students. So if you arrive too late, you can only enter the room when the quiz is done.
\end{itemize}

\subsection{Schedule}

\begin{table}[h]
    \centering
    \begin{tabularx}{\textwidth}[b]{|l|l|X|X|l|}
        \hline
        Wk & T/P & Topic & Deliverable & Quiz \\
        \hline \hline
        1.1 & Theory & The basics:
            \newline ADC, sampling, aliasing, Nyquist & & \\ 
        \hline
        1.2 & Theory & Discrete time systems:
            \newline LTI, difference equation, impulse response & & 1 \\ 
        \hline
        1.3 & Project & Define your DSP project & & 2\\
        \hline
        1.4 & Project & Signal acquisition: 
            \newline ADC, sample frequency, pre- and postconditioning circuitry & Introduction 
            \newline Problem statement & \\
        \hline
        1.5 & Theory & The z-domain:
            \newline z-transform, z-plane &  & \\ 
        \hline
        1.6 & Theory & The z-domain:
            \newline Poles and zeroes, stability & & \\ 
        \hline
        1.7 & Project & Recap C and MCU:
            \newline implementation & Block diagram 
            \newline Signal acquisition & 3\\
        \hline
        \hline
        2.1 & Theory & The frequency domain:
            \newline Fourier transform, Nyquist & &\\ 
        \hline
        2.2 & Theory & The frequency domain:
            \newline Transfer functions & & \\ 
        \hline
        2.3 & Theory & Filter design:
            \newline Analog templates, biquad & & 4\\ 
        \hline
        2.4 & Project & Design your DSP system 
            \newline Derive specs from project, design with software tools & Filter design and analysis & \\
        \hline
        2.5 & Project & Implement your system & & 5 \\
        \hline
        2.6 & Project & Test and document:
            \newline Measure signals and spectra and produce plots & & \\
        \hline        
        2.7 & Project & Present your DSP design:
            \newline Show working demo to the group and answer questions &  & \\
        \hline
        2.8 & & Deadline & Final portfolio & \\
        \hline

    \end{tabularx}
    \label{tab:workshop_schedule}
\end{table}

\begin{table}[h]
    \centering
    \begin{tabularx}{\textwidth}[b]{|l|l|X|X|}
        \hline
        Wk & Deliverable & "InleverApp" deadline & "InleverApp" re-sit \\
        \hline \hline
        1.4 & Introduction + Problem statement & 08/03 & 22/03 \\
        \hline
        1.7 & Block diagram + Signal acquisition & 29/03 & 12/04 \\
        \hline
        2.4 & Filter design and analysis & 17/05 & 31/05 \\
        \hline        
        2.7 & Show and tell &   &  \\
        \hline
        2.8 & Final portfolio & 14/06 & 05/07 \\
        \hline
    \end{tabularx}
\end{table}

\newpage

\section{Matlab}

It is strongly advised to refresh your coding skills prior to the start of the workshop. Furtheron, we will be using small bits of Matlab scripts as examples throughout the workshop.
Have a look at MATLAB \textbf{Onramp} \url{https://matlabacademy.mathworks.com/R2019b/portal.html?course=gettingstarted} and take the basic course where you will learn interactively using MATLAB.



\section{More information}
There is a vast number of sources on DSP available on the internet. As an example, a couple of interesting video clips are referenced in this list:
\begin{itemize}
    \item Digital signals intro: \url{http://www.youtube.com/watch?v=btgAUdbj85E}
    \item Sampling, aliasing and Nyquist: \url{http://www.youtube.com/watch?v=yWqrx08UeUs}
    \item Phase angles: \url{http://www.youtube.com/watch?v=pGa1G_sCZAk&feature=youtu.be}
    \item Moving average filters: \url{http://www.youtube.com/watch?v=loHy8v9A8LY\&t=5m35s}
    \item Fourier transform: \url{http://www.youtube.com/watch?v=ZRZIz81nXo4}
    \item Fourier transform: \url{http://www.youtube.com/watch?v=1JnayXHhjlg}
    \item z-plane: \url{http://www.youtube.com/watch?v=iO0Wg5BxGLw}
\end{itemize}
Even more interesting is the large amount of material available online, e.g.
\begin{itemize}
    \item \url{http://en.wikipedia.org/wiki/Signal_processing}
    \item \url{http://en.wikibooks.org/wiki/Signals_and_Systems}
    \item \url{http://www.dspguide.com}
    \item \url{http://eceweb1.rutgers.edu/~orfanidi/intro2sp/}
    \item \url{http://www.allaboutcircuits.com/technical-articles/an-introduction-to-digital-signal-processing/}
    \item \url{http://ocw.mit.edu/resources/res-6-008-digital-signal-processing-spring-2011/}
    \item \url{http://training.ti.com/signal-processing-overview}
    \item \url{http://nl.mathworks.com/help/signal/getting-started-with-signal-processing-toolbox}
    \item \url{http://behindthesciences.com/signal-processing/fouriertransformmatlabtutorial}
\end{itemize}
You are encouraged to practice your information searching skills by finding relevant sources and sharing those in the workshop.

